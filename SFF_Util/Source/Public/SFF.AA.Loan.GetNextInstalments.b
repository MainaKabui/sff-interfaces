* @ValidationCode : MjotOTYzMzUzODk2OkNwMTI1MjoxNjExNTk1NTI2ODEyOmt1ZHphbmFpLm1hc2l3YTotMTotMTowOjA6ZmFsc2U6Ti9BOlIxOV9TUDEyLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 25 Jan 2021 19:25:26
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : kudzanai.masiwa
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP12.0
$PACKAGE SFF.Util
SUBROUTINE SFF.AA.Loan.GetNextInstalments(ArrangementId,repaymentAmount,repaymentDate)
*-----------------------------------------------------------------------------
*
*-----------------------------------------------------------------------------
* Modification History :
*-----------------------------------------------------------------------------

*-----------------------------------------------------------------------------
    $USING AA.PaymentSchedule
    $USING AA.ProductFramework
    $USING EB.SystemTables
    DUE.DATES = ''  ;* Holds the list of Schedule due dates
    DUE.TYPES = ''  ;* Holds the list of Payment Types for the above dates
    DUE.TYPE.AMTS = ''        ;* Holds the Payment Type amounts
    DUE.PROPS = ''  ;* Holds the Properties due for the above type
    DUE.PROP.AMTS = ''        ;* Holds the Property Amounts for the Properties above
    DUE.OUTS = ''   ;* Oustanding Bal for the date
    DUE.METHODS = ""

    SCHED.ARR = ''

    ARR.ID = ArrangementId

    DATE.REQD = ''
    CYCLE.DATE = ''
    SIM.REF = ''

    AA.PaymentSchedule.ScheduleProjector(ARR.ID, SIM.REF, "",CYCLE.DATE, TOT.PAYMENT, DUE.DATES, DEFER.DATES, DUE.TYPES, DUE.METHODS, DUE.TYPE.AMTS, DUE.PROPS, DUE.PROP.AMTS, DUE.OUTS)      ;* Routine to Project complete schedules
    
    GOSUB CheckDates
    
    returnData = 0
    vcnt = DCOUNT(dueAmts, @VM)
    
    FOR vv = 1 TO vcnt
        returnData += dueAmts<1,vv>
    NEXT vv
    
RETURN

CheckDates:
***********
    fcnt = DCOUNT(DUE.DATES,@FM)
    dueAmts = 0
    FOR ff = 1 TO fcnt
        dueDate = DUE.DATES<ff>
        IF dueDate GE EB.SystemTables.getToday() THEN
            dueAmts = DUE.PROP.AMTS<ff>
            RETURN
        END
    NEXT ff
RETURN
END
