* @ValidationCode : Mjo2MTQ2NjcyOTc6Q3AxMjUyOjE2MDAzODg1NzMwMzk6a3VkemFuYWkubWFzaXdhOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjE5X1NQMTIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 18 Sep 2020 02:22:53
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : kudzanai.masiwa
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP12.0
$PACKAGE SFF.Util
FUNCTION SGNT.PARSE.PAYLOAD(payloadresponse,field)
*-----------------------------------------------------------------------------
*
*-----------------------------------------------------------------------------
* Modification History :
*-----------------------------------------------------------------------------

*-----------------------------------------------------------------------------
 IF INDEX(payloadresponse,field,1) THEN
        
        start = INDEX(payloadresponse,field,1) + LEN(field)
        result = FIELD(payloadresponse,field,2)
        result = FIELD(result,'"',2)
 
 END
RETURN result
END
