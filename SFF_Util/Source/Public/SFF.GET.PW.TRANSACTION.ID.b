* @ValidationCode : Mjo4NjU3ODY5NjE6Q3AxMjUyOjE1OTk1NjY3NjE1MDQ6TWFpbmE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMTlfU1AyNC4wOi0xOi0x
* @ValidationInfo : Timestamp         : 08 Sep 2020 15:06:01
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.Util
*
* Implementation of SFF.Util.GetPwTransactionId
*
* activityId(IN) :
* error(IN) :
*
FUNCTION SFF.GET.PW.TRANSACTION.ID(activityId, error)
    $USING PW.Foundation
              
    rec = PW.Foundation.ActivityTxn.Read(activityId, err)
    IF err THEN
        error = err
        RETURN
    END
    
    processId = rec<PW.Foundation.ActivityTxn.ActTxnProcess>
    rec = PW.Foundation.Process.Read(processId, err)
    IF err THEN
        error = err
        RETURN
    END
    
    activityId = rec<PW.Foundation.Process.ProcActivityTxn,1>
    rec = PW.Foundation.ActivityTxn.Read(activityId, err)
    IF err THEN
        error = err
        RETURN
    END
    
    transactionId = rec<PW.Foundation.ActivityTxn.ActTxnTransactionRef>
RETURN (transactionId)

END