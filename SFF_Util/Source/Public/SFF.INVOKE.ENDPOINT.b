* @ValidationCode : MjoyMDgwODE3NTc1OkNwMTI1MjoxNTk3NzkyMjQ3MTc1Ok1haW5hOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjE5X1NQMjQuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Aug 2020 02:10:47
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.Util
*
* Implementation of SFF.Util.InvokeEndPoint
*
* request(IN) :
*
FUNCTION SFF.INVOKE.ENDPOINT(request)

    GOSUB Init
RETURN(result)

Init:
    result = ""
    CALL SFF.GENERIC.LISTENER(request, result)
RETURN

END