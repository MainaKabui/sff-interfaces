* @ValidationCode : MjotMzkxMDQ5Nzg6Q3AxMjUyOjE1OTc3Nzg1MjkwMDA6TWFpbmE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMTlfU1AyNC4wOi0xOi0x
* @ValidationInfo : Timestamp         : 18 Aug 2020 22:22:09
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MonCash
*
* Implementation of SFF.MonCash.SendCheckRtn
*
*
SUBROUTINE V.SFF.MC.SEND.CHECK.RTN
    $USING EB.SystemTables
  
    tmpT<3> = "NOINPUT"
    EB.SystemTables.setT(SFF.MonCash.MonCash.AccountNo, tmpT)
    
    tmpN = "36..C"
    EB.SystemTables.setN(SFF.MonCash.MonCash.AccountNo, tmpN)
RETURN

END
