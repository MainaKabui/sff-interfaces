* @ValidationCode : MjotNTg3OTI0NjczOkNwMTI1MjoxNTk5Nzc4Njc1MTk5Ok1haW5hOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjE5X1NQMjQuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 11 Sep 2020 01:57:55
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MonCash
*
* Implementation of SFF.MonCash.AuthRtn
*
*
SUBROUTINE V.SFF.MC.AUTH.RTN
    $USING PW.Foundation
    $USING EB.SystemTables
    $USING PW.API
    $USING SFF.Util
    $USING FT.Contract
    
    GOSUB Init
RETURN
    
Init:
    t24Ref = EB.SystemTables.getIdNew()
    activityId = PW.Foundation.getActivityTxnId()
                   
    GOSUB GetMonCashData
RETURN

GetMonCashData:
    IF NOT(activityId) THEN
        EB.SystemTables.setE("Invalid Activity Id")
        EB.SystemTables.setAf("")
        RETURN
    END
                     
    monCashId = SFF.Util.GetPwTransactionId(activityId, error)
    IF error THEN
        EB.SystemTables.setE(error)
        EB.SystemTables.setAf("")
        RETURN
    END
    
    rec = SFF.MonCash.MonCash.Read(monCashId, error)
    IF error THEN
        EB.SystemTables.setE(error)
        EB.SystemTables.setAf("")
        RETURN
    END
    
    msisdn = rec<SFF.MonCash.MonCash.B2CTo>
    amount = rec<SFF.MonCash.MonCash.B2CAmount>
    description = rec<SFF.MonCash.MonCash.B2CDescription>
        
    GOSUB MTransfer
RETURN

MTransfer:
    SFF.MonCash.MTransfer(monCashId, msisdn, amount, description, transId, error)
    IF error THEN
        EB.SystemTables.setE(error)
        RETURN
    END
    
    GOSUB UpdateFields
RETURN

UpdateFields:
    rec<SFF.MonCash.MonCash.T24TxnRef> = t24Ref
    rec<SFF.MonCash.MonCash.B2CTransId> = transId
    SFF.MonCash.MonCash.Write(clientTransRef, rec)
RETURN

END