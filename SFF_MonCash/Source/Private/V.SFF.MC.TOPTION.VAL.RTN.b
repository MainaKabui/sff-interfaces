* @ValidationCode : MjotMzkzOTc1NDIyOkNwMTI1MjoxNTk3ODQxODI5NTEzOk1haW5hOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjE5X1NQMjQuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Aug 2020 15:57:09
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MonCash
*
* Implementation of SFF.MonCash.TransferOptValidationRtn
*
*
SUBROUTINE V.SFF.MC.TOPTION.VAL.RTN
    $USING EB.SystemTables

    GOSUB Init
    GOSUB Process
RETURN

Init:
    transferOption = EB.SystemTables.getComi()
RETURN

Process:
    tmpT = EB.SystemTables.getT(SFF.MonCash.MonCash.AccountNo)
    tmpN = EB.SystemTables.getN(SFF.MonCash.MonCash.AccountNo)
    IF transferOption EQ "ACCOUNT" THEN
        tmpT<3> = ""
        tmpN = "36.1.C"
    END ELSE
        tmpT<3> = "NOINPUT"
        tmpN = "36..C"
    END
    EB.SystemTables.setT(SFF.MonCash.MonCash.AccountNo, tmpT)
    EB.SystemTables.setN(SFF.MonCash.MonCash.AccountNo, tmpN)
RETURN

END