* @ValidationCode : MjotMTk4MzgzMjMzOkNwMTI1MjoxNTk4MDUyNTEwODI0Ok1haW5hOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjE5X1NQMjQuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 22 Aug 2020 02:28:30
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MonCash
*
* Implementation of SFF.MonCash.SendBeforeAuthRtn
*
*
SUBROUTINE V.SFF.MC.SEND.BAUTH.RTN

    $USING EB.SystemTables
    $USING TT.Contract
    $USING FT.Contract
    $USING EB.Foundation
    $USING EB.Interface
    
    GOSUB Init
RETURN
    
Init:
    amount = EB.SystemTables.getRNew(SFF.MonCash.MonCash.B2CAmount)
    transferOption = EB.SystemTables.getRNew(SFF.MonCash.MonCash.TransferOption)
    ccy = "HTG"
    
    ofsId = SFF.MonCash.GetCommon("OFS.ID")
    IF NOT(ofsId) THEN
        EB.SystemTables.setE("Missing OFS.ID")
        RETURN
    END
    
    category = SFF.MonCash.GetCommon("CASH.CATEGORY")
    IF NOT(category) THEN
        EB.SystemTables.setE("Missing the Cash Category")
        RETURN
    END
    
    GOSUB GetCreditAccount
RETURN

GetCreditAccount:
    creditAccount = SFF.MonCash.GetCommon("COLLECTION.ACCT")
    IF NOT(creditAccount) THEN
        EB.SystemTables.setE("Missing MoneyGram's Collection Account")
        RETURN
    END
    
    GOSUB GetDebitAccount
RETURN

GetDebitAccount:
    IF transferOption EQ "CASH" THEN
        transactionType = SFF.MonCash.GetCommon("TT.TRANSACTION")
        currentUser = EB.SystemTables.getOperator()
        rec = TT.Contract.TellerUser.CacheRead(currentUser, err)
        IF err THEN
            EB.SystemTables.setE("Sorry you do not have a Till")
            RETURN
        END
    
        tellerId = rec<1>
        debitAccount = ccy:category:tellerId
        
        appName = "TELLER"
        versionName = "TELLER,SFF.MONCASH"
        transRec<TT.Contract.Teller.TeTransactionCode> = transactionType
        transRec<TT.Contract.Teller.TeTellerIdOne> = tellerId
        transRec<TT.Contract.Teller.TeAccountOne> = debitAccount
        transRec<TT.Contract.Teller.TeAmountLocalOne> = amount
        transRec<TT.Contract.Teller.TeCurrencyOne> = ccy
        transRec<TT.Contract.Teller.TeAccountTwo> = creditAccount
    END ELSE
        appName = "FUNDS.TRANSFER"
        versionName = "FUNDS.TRANSFER,SFF.MONCASH"
        debitAccount = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.AccountNo)
        transactionType = SFF.MonCash.GetCommon("FT.TRANSACTION")
        transRec<FT.Contract.FundsTransfer.TransactionType> = transactionType
        transRec<FT.Contract.FundsTransfer.DebitAcctNo> = debitAccount
        transRec<FT.Contract.FundsTransfer.DebitAmount> = amount
        transRec<FT.Contract.FundsTransfer.DebitCurrency> = ccy
        transRec<FT.Contract.FundsTransfer.CreditAcctNo> = creditAccount
        transRec<FT.Contract.FundsTransfer.OrderingBank> = SFF.MonCash.BankName
    END
        
    GOSUB PostOfs

RETURN

PostOfs:
    ofsFunc = "I"
    process = "PROCESS"
    gtsMode = ""
    noOfAuth = 0
    EB.Foundation.OfsBuildRecord(appName, ofsFunc, process, versionName, gtsMode, noOfAuth, transactionId, transRec, ofsReq)
    
    insertOrAdd = 'ADD'
    EB.Interface.OfsAddlocalrequest(ofsReq, insertOrAdd, ofsError)
    IF ofsError THEN
        EB.SystemTables.setE(ofsError)
        RETURN
    END
                
    GOSUB SaveSession
RETURN

SaveSession:
    SFF.MonCash.setMonCashId(EB.SystemTables.getIdNew())
RETURN

END