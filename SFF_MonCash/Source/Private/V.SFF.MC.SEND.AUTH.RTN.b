* @ValidationCode : MjoxODg3MzQ1MDY1OkNwMTI1MjoxNTk4MDQ3NTU5NTM0Ok1haW5hOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjE5X1NQMjQuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 22 Aug 2020 01:05:59
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MonCash
*
* Implementation of SFF.MonCash.SendTTAuthRtn
*
*
SUBROUTINE V.SFF.MC.SEND.AUTH.RTN
    $USING EB.SystemTables
    $USING TT.Contract
    
    GOSUB Init
RETURN
        
Init:
    clientTransRef = SFF.MonCash.getMonCashId()
                         
    GOSUB GetMonCashData
RETURN

GetMonCashData:
    rec = SFF.MonCash.MonCash.Read(clientTransRef, error)
    msisdn = rec<SFF.MonCash.MonCash.B2CTo>
    amount = rec<SFF.MonCash.MonCash.B2CAmount>
    description = rec<SFF.MonCash.MonCash.B2CDescription>
            
    GOSUB MTransfer
RETURN

MTransfer:
    SFF.MonCash.MTransfer(clientTransRef, msisdn, amount, description, transId, error)
    IF error THEN
        EB.SystemTables.setE(error)
        RETURN
    END
    
    GOSUB UpdateFields
RETURN

UpdateFields:
    rec<SFF.MonCash.MonCash.B2CTransId> = transId
    SFF.MonCash.MonCash.Write(clientTransRef, rec)
RETURN
    
END