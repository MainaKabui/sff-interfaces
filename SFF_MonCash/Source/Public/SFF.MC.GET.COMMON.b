* @ValidationCode : MjotNzQyMjc5NDYxOkNwMTI1MjoxNTk3NzkwMjcxNzk4Ok1haW5hOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjE5X1NQMjQuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Aug 2020 01:37:51
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MonCash
*
* Implementation of SFF.MonCash.GetCommon
*
* key(IN) :
*
FUNCTION SFF.MC.GET.COMMON(key)
    $USING SFF.Util
    
    GOSUB Init
RETURN (val)
    
Init:
    rec = SFF.Util.SFFCommon.CacheRead("MONCASH", err)
    keys = rec<SFF.Util.SFFCommon.ParameterName>
    val = ""

    GOSUB GetValue
RETURN

GetValue:
    CHANGE @VM TO @FM IN keys
    LOCATE key IN keys SETTING pos THEN
        val = rec<SFF.Util.SFFCommon.ParameterValue, pos>
    END
RETURN
    
END