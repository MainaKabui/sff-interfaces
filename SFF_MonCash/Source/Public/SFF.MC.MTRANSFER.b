* @ValidationCode : MjoxMTExNTI1NzkxOkNwMTI1MjoxNTk4Mjk5OTk4MzkwOk1haW5hOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjE5X1NQMjQuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 24 Aug 2020 23:13:18
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MonCash
*
* Implementation of SFF.MonCash.MTransfer
*
* clientTransRef(IN) :
* msisdn(IN) :
* amount(IN) :
* description(IN) :
* transId(OUT) :
* error(OUT) :
*
SUBROUTINE SFF.MC.MTRANSFER(clientTransRef, msisdn, amount, description, transId, error)
    $USING SFF.Util
    
    GOSUB Init
RETURN
    
Init:
    IF NOT(clientTransRef) THEN
        error = "Missing T24 Ref"
        RETURN
    END
     
    IF NOT(msisdn) THEN
        error = "Missing Mobile Number"
        RETURN
    END

    IF NOT(amount) THEN
        error = "Missing Amount"
        RETURN
    END

    endPoint = SFF.MonCash.GetCommon("ENDPOINT")
    IF NOT(endPoint) THEN
        error = "Missing ENDPOINT"
        RETURN
    END
    
    connectionTimeout  = SFF.MonCash.GetCommon("CONNECTION.TIMEOUT")
    IF NOT(connectionTimeout) THEN
        error = "Missing CONNECTION.TIMEOUT"
        RETURN
    END
    
    sessionTimeout  = SFF.MonCash.GetCommon("SESSION.TIMEOUT")
    IF NOT(sessionTimeout) THEN
        error = "Missing SESSION.TIMEOUT"
        RETURN
    END
    
    userName = SFF.MonCash.GetCommon("USERNAME")
    IF NOT(userName) THEN
        error = "Missing USERNAME"
        RETURN
    END
    
    password = SFF.MonCash.GetCommon("PASSWORD")
    IF NOT(password) THEN
        error = "Missing PASSWORD"
        RETURN
    END
    
    timeZone = SFF.MonCash.GetCommon("TIMEZONE")
    IF NOT(timeZone) THEN
        error = "Missing TIMEZONE"
        RETURN
    END
    
    dateFormat = SFF.MonCash.GetCommon("DATE.FORMAT")
    IF NOT(dateFormat) THEN
        error = "Missing DATE.FORMAT"
        RETURN
    END
    
    terminalId = SFF.MonCash.GetCommon("TERMINAL.ID")
    IF NOT(terminalId) THEN
        error = "Missing TERMINAL.ID"
        RETURN
    END
    
    clientId = SFF.MonCash.GetCommon("CLIENT.ID")
    IF NOT(clientId) THEN
        error = "Missing CLIENT.ID"
        RETURN
    END
    
    host = SFF.MonCash.GetCommon("HOST")
    IF NOT(host) THEN
        error = "Missing HOST"
        RETURN
    END
    
    GOSUB BuildPayload
RETURN

BuildPayload:
    request = 'password*':password:'|dateFormat*':dateFormat:'|timeZone*':timeZone:'|requestType*getMonCashData'
    monCashData = SFF.Util.InvokeEndPoint(request)
    IF NOT(monCashData) THEN
        error = "Invalid MonCash Data"
        RETURN
    END
   
    statusCode = LEFT(monCashData,2)
    statusMessage = RIGHT(monCashData, LEN(monCashData)-3)
    IF statusCode NE "00" THEN
        error = statusMessage
        RETURN
    END
  
    passwordDigest = FIELD(statusMessage, '|', 1)
    baseNoonce = FIELD(statusMessage, '|', 2)
    created = FIELD(statusMessage, '|', 3)
   
    payload = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://www.amdocs.com/xml/ns/card/soap/v1" xmlns:sen="http://api.com.amdocs.dc.cms/SensitiveDetails/" xmlns:v3="http://www.utiba.com/xml/ns/sdp/soap/service/v3">'
    payload := '<soapenv:Header>'
    payload := '<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">'
    payload := '<wsse:UsernameToken>'
    payload := '<wsse:Username>':userName:'</wsse:Username>'
    payload := '<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">':passwordDigest:'</wsse:Password>'
    payload := '<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">':baseNoonce:'</wsse:Nonce>'
    payload := '<wsu:Created>':created:'</wsu:Created>'
    payload := '</wsse:UsernameToken>'
    payload := '</wsse:Security>'
    payload := '<v3:terminalId>':terminalId:'</v3:terminalId>'
    payload := '<v3:clientId>':clientId:'</v3:clientId>'
    payload := '</soapenv:Header>'
    payload := '<soapenv:Body>'
    payload := '<v3:mtransferRequest>'
    payload := '<v3:clientTransRef>':clientTransRef:'</v3:clientTransRef>'
    payload := '<v3:amount>':amount:'</v3:amount>'
    payload := '<v3:to>':msisdn:'</v3:to>'
    payload := '<v3:description>':description:'</v3:description>'
    payload := '</v3:mtransferRequest>'
    payload := '</soapenv:Body>'
    payload := '</soapenv:Envelope>'
   
    GOSUB BuildHeaders
RETURN

BuildHeaders:
    headers = 'Content-Type: text/xml;charset=UTF-8'
    headers := '^Content-Length:':LEN(payload)
    headers := '^Host:':host
    GOSUB SendRequest
RETURN

SendRequest:
    request = 'endPoint*':endPoint
    request := '|connectionTimeout*':connectionTimeout
    request := '|sessionTimeout*':sessionTimeout
    request := '|headers*':headers
    request := '|payload*':payload
    request := '|requestType*postRequest'
   
    response = SFF.Util.InvokeEndPoint(request)
    statusCode = LEFT(response,2)
    statusMessage = RIGHT(response, LEN(response)-3)
    IF statusCode NE "00" THEN
        error = statusMessage
        RETURN
    END
      
    statusCode = "05"
    IF INDEX(statusMessage,"<faultstring>",1) THEN
        str = "<faultstring>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</faultstring>",1)
        c = b - a
        error = statusMessage[a,c]
        RETURN
    END
    
    IF INDEX(statusMessage,"<status>",1) THEN
        str = "<status>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</status>",1)
        c = b - a
        status = statusMessage[a,c]
    END ELSE
        error = "Invalid MonCash status"
        RETURN
    END
    
    IF status NE "successful" THEN
        error = "MonCash error: ":status
        RETURN
    END
    
    IF INDEX(statusMessage,"<transId>",1) THEN
        str = "<transId>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</transId>",1)
        c = b - a
        transId = statusMessage[a,c]
    END ELSE
        error = "Fatal error. Missing MonCash transId"
    END
              
RETURN

END