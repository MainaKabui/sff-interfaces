* @ValidationCode : MjotMjIzMTA0ODU5OkNwMTI1MjoxNjAwMjg1MTc3Njc4Omt1ZHphbmFpLm1hc2l3YTotMTotMTowOjA6ZmFsc2U6Ti9BOlIxOV9TUDYuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 16 Sep 2020 21:39:37
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : kudzanai.masiwa
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP6.0
$PACKAGE SFF.Lajan
FUNCTION SFF.LAJAN.GET.COMMON(key)
*-----------------------------------------------------------------------------
*
*-----------------------------------------------------------------------------
* Modification History :
*-----------------------------------------------------------------------------

*-----------------------------------------------------------------------------
    
    $USING SFF.Util
    
    GOSUB Init
RETURN (val)

Init:
    rec = SFF.Util.SFFCommon.CacheRead("LAJANCASH", err)
    keys = rec<SFF.Util.SFFCommon.ParameterName>
    val = ""

    GOSUB GetValue
RETURN

GetValue:
    CHANGE @VM TO @FM IN keys
    LOCATE key IN keys SETTING pos THEN
        val = rec<SFF.Util.SFFCommon.ParameterValue, pos>
    END
RETURN
    
END