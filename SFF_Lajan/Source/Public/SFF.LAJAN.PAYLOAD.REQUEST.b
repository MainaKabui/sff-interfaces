* @ValidationCode : MjoxOTIyODIxOTkzOkNwMTI1MjoxNjAwOTgwNjUxNjQyOmt1ZHphbmFpLm1hc2l3YTotMTotMTowOjA6ZmFsc2U6Ti9BOlIxOV9TUDEyLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 24 Sep 2020 22:50:51
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : kudzanai.masiwa
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP12.0
$PACKAGE SFF.Lajan
SUBROUTINE SFF.LAJAN.PAYLOAD.REQUEST(clientIdentifierType, clientIdentifierValue, amount, description, tranType, tranStatus ,tranData, tranRespData, error)
*-----------------------------------------------------------------------------
*
*-----------------------------------------------------------------------------
* Modification History :
*-----------------------------------------------------------------------------

*-----------------------------------------------------------------------------
    $USING SFF.Util
    

    GOSUB Init
    
    
RETURN

Init:
    sep = "~"
    otp = tranData[sep,6,1]
    IF NOT(clientIdentifierType) THEN
        error = "Missing Client Identifier Type"
        RETURN
    END
     
    IF NOT(clientIdentifierValue) THEN
        error = "Missing Client Identifier Value"
        RETURN
    END

    IF NOT(amount) THEN
        error = "Missing Amount"
        RETURN
    END

    baseUri = SFF.Lajan.GetCommon("BASE.URI")
    IF NOT(baseUri) THEN
        error = "Missing BASE URI"
        RETURN
    END
    
    connectionTimeout  = SFF.Lajan.GetCommon("CONNECTION.TIMEOUT")
    IF NOT(connectionTimeout) THEN
        error = "Missing CONNECTION TIMEOUT"
        RETURN
    END
    
    sessionTimeout  = SFF.Lajan.GetCommon("SESSION.TIMEOUT")
    IF NOT(sessionTimeout) THEN
        error = "Missing SESSION TIMEOUT"
        RETURN
    END
    
    timeZone = SFF.Lajan.GetCommon("TIMEZONE")
    IF NOT(timeZone) THEN
        error = "Missing TIMEZONE"
        RETURN
    END
    
    dateFormat = SFF.Lajan.GetCommon("DATE.FORMAT")
    IF NOT(dateFormat) THEN
        error = "Missing DATE.FORMAT"
        RETURN
    END
    
    oAuthclientId = SFF.Lajan.GetCommon("OAUTHCLIENTID")
    IF NOT(oAuthclientId) THEN
        error = "Missing OAUTHCLIENTID"
        RETURN
    END
    
    oAuthclientSecret = SFF.Lajan.GetCommon("OAUTHCLIENTSECRET")
    IF NOT(oAuthclientSecret) THEN
        error = "Missing OAUTHCLIENTSECRET"
        RETURN
    END
    
    scope = SFF.Lajan.GetCommon("SCOPE")
    IF NOT(scope) THEN
        error = "Missing SCOPE"
        RETURN
    END ELSE
        CHANGE "." TO "_" IN scope
        CHANGE @SM TO SPACE(1) IN scope
    END
    
    tokenApi = SFF.Lajan.GetCommon("TOKEN.API")
    IF NOT(tokenApi) THEN
        error = "Missing TOKEN.API"
        RETURN
    END
    
    BEGIN CASE
        CASE tranType EQ "CASHIN"
            
            cashInPreAuthApi = SFF.Lajan.GetCommon("CASHIN.PREAUTH.API")
            IF NOT(cashInPreAuthApi) THEN
                error = "Missing CASHIN.PREAUTH.API"
                RETURN
            END
        
            cashInMakeApi = SFF.Lajan.GetCommon("CASHIN.MAKE.API")
            IF NOT(cashInMakeApi) THEN
                error = "Missing CASHIN.MAKE.API"
                RETURN
            END
    
        CASE tranType EQ "CASHOUT"
        
            cashOutPreAuthApi = SFF.Lajan.GetCommon("CASHOUT.PREAUTH.API")
            IF NOT(cashOutPreAuthApi) THEN
                error = "Missing CASHIN.PREAUTH.API"
                RETURN
            END
        
            cashOutMakeApi = SFF.Lajan.GetCommon("CASHOUT.MAKE.API")
            IF NOT(cashOutMakeApi) THEN
                error = "Missing CASHIN.MAKE.API"
                RETURN
            END
        CASE 1
    END CASE
    
    authPinAcceptor = SFF.Lajan.GetCommon("AUTH.PIN.ACCEPTOR")
    IF NOT(authPinAcceptor) THEN
        error = "Missing AUTH.PIN.ACCEPTOR"
        RETURN
    END
    
    authSendOtp = SFF.Lajan.GetCommon("AUTH.SEND.OTP")
    IF NOT(authSendOtp) THEN
        error = "Missing AUTH.SEND.OTP"
        RETURN
    END
    
    authVerifyOtp = SFF.Lajan.GetCommon("AUTH.VERIFY.OTP")
    IF NOT(authVerifyOtp) THEN
        error = "Missing AUTH.VERIFY.OTP"
        RETURN
    END
    
    pinAcceptorpin = SFF.Lajan.GetCommon("PIN.ACCEPTOR.PIN")
    IF NOT(pinAcceptorpin) THEN
        error = "Missing PIN.ACCEPTOR.PIN"
        RETURN
    END
    
    
    GOSUB PROCESS.Payload
RETURN

PROCESS.Payload:
    BEGIN CASE
        CASE tranType EQ "CASHIN" AND tranStatus EQ "PREAUTH"
             
            GOSUB BUILD.tokenPayload
            GOSUB BUILD.tokenHeader
            GOSUB EXECUTE.payLoad
            field = '"access_token"'
            
            GOSUB PARSE.payload
            access_token = value
            
            GOSUB BUILD.cashInPreAuthPayload
            GOSUB BUILD.payloadHeader
            GOSUB EXECUTE.payLoad
            
            field = '"token"'
            GOSUB PARSE.payload
            token = value
            
            field = '"date"'
            GOSUB PARSE.payload
            datetime = value
            tranRespData = token :sep: datetime
            
        CASE tranType EQ "CASHIN" AND tranStatus EQ "AUTH"
        
            GOSUB BUILD.tokenPayload
            GOSUB BUILD.tokenHeader
            GOSUB EXECUTE.payLoad
            field = '"access_token"'
            GOSUB PARSE.payload
            access_token = value
            token = tranData[sep,1,1]
            GOSUB BUILD.authenticatePinAcceptorPayLoad
            GOSUB BUILD.AuthenticationHeader
            GOSUB EXECUTE.payLoad
            
            field = '"status"'
            GOSUB PARSE.payload
            authStatus = value
            
            field = '"token"'
            GOSUB PARSE.payload
            token = value
            CRT authStatus :sep: token
            
        CASE tranType EQ "CASHIN" AND tranStatus EQ "COMPLETE"
        
            GOSUB BUILD.tokenPayload
            GOSUB BUILD.tokenHeader
            GOSUB EXECUTE.payLoad
            field = '"access_token"'
            
            GOSUB PARSE.payload
            access_token = value
            token = tranData[sep,1,1]
            GOSUB BUILD.cashInMakePayLoad
            GOSUB BUILD.payloadHeader
            GOSUB EXECUTE.payLoad
            
            field = '"id"'
            GOSUB PARSE.payload
            transId = value
            
            field = '"date"'
            GOSUB PARSE.payload
            datetime = value
            tranRespData = transId :sep: datetime
            
        CASE tranType EQ "CASHOUT" AND tranStatus EQ "PREAUTH"
                   
            GOSUB BUILD.tokenPayload
            GOSUB BUILD.tokenHeader
            GOSUB EXECUTE.payLoad
            field = '"access_token"'
            
            GOSUB PARSE.payload
            access_token = value
            GOSUB BUILD.cashOutPreAuthPayload
            GOSUB BUILD.payloadHeader
            GOSUB EXECUTE.payLoad
            
            field = '"token"'
            GOSUB PARSE.payload
            token = value
            
            field = '"date"'
            GOSUB PARSE.payload
            datetime = value
            
            GOSUB BUILD.authenticateSendOTPPayLoad
            GOSUB BUILD.AuthenticationHeader
            GOSUB EXECUTE.payLoad
            
            IF error THEN RETURN
            
            field = '"status"'
            GOSUB PARSE.payload
            authStatus = value
            
            tranRespData =  token :sep: datetime :sep: authStatus
        CASE tranType EQ "CASHOUT" AND tranStatus EQ "AUTH"
        
            IF NOT(otp) THEN
                error = "Client OTP Required for CASHOUT Authorisation"
                RETURN
            END
            
            GOSUB BUILD.tokenPayload
            GOSUB BUILD.tokenHeader
            GOSUB EXECUTE.payLoad
            
            IF error THEN RETURN
            
            field = '"access_token"'
            GOSUB PARSE.payload
            access_token = value
            token = tranData[sep,1,1]
            GOSUB BUILD.authenticatePinAcceptorPayLoad
            GOSUB BUILD.AuthenticationHeader
            GOSUB EXECUTE.payLoad
            
            IF error THEN RETURN
            
            field = '"status"'
            GOSUB PARSE.payload
            authStatus = value
            
            field = '"token"'
            GOSUB PARSE.payload
            token = value
            
            GOSUB BUILD.authenticateVerifyOTPPayLoad
            GOSUB BUILD.AuthenticationHeader
            GOSUB EXECUTE.payLoad
            
            field = '"status"'
            GOSUB PARSE.payload
            authStatus = value
            
            field = '"token"'
            GOSUB PARSE.payload
            token = value
            tranRespData =  token
        CASE tranType EQ "CASHOUT" AND tranStatus EQ "COMPLETE"
              
            GOSUB BUILD.tokenPayload
            GOSUB BUILD.tokenHeader
            GOSUB EXECUTE.payLoad
            field = '"access_token"'
            
            GOSUB PARSE.payload
            access_token = value
            token = tranData[sep,1,1]
            otp = tranData[sep,2,1]
            
            GOSUB BUILD.cashOutMakePayLoad
            GOSUB BUILD.payloadHeader
            GOSUB EXECUTE.payLoad
            
            field = '"id"'
            GOSUB PARSE.payload
            transId = value
            
            field = '"date"'
            GOSUB PARSE.payload
            datetime = value
            tranRespData = transId :sep: datetime
    END CASE
   
RETURN

EXECUTE.payLoad:
    GOSUB SendRequest
RETURN

BUILD.payloadHeader:
    headers = 'Content-Type: application/json'
    headers := '^Authorization: Bearer ':access_token
      
RETURN

BUILD.tokenHeader:
    headers = 'Content-Type: application/json'
RETURN

BUILD.AuthenticationHeader:
    headers = 'Content-Type: application/json'
    headers := '^Authorization: Bearer ':access_token
    headers := '^X-HTTP-Method-Override: PATCH'
RETURN


SendRequest:
    request = 'endPoint*':endPoint
    request := '|connectionTimeout*':connectionTimeout
    request := '|sessionTimeout*':sessionTimeout
    request := '|headers*':headers
    request := '|payload*':payload
    request := '|requestType*postRequest'
   
    response = SFF.Util.InvokeEndPoint(request)
    statusCode = LEFT(response,2)
    statusMessage = RIGHT(response, LEN(response)-3)
    IF statusCode NE "00" THEN
        error = statusMessage
        RETURN
    END
 
              
RETURN

BUILD.tokenPayload:
    tokenpayload = ""
    tokenpayload = '{"grant_type": "client_credentials",'
    tokenpayload := '"scope": "':scope:'",'
    tokenpayload := '"client_id": "':oAuthclientId:'",'
    tokenpayload :=  '"client_secret": "':oAuthclientSecret:'"}'
            
    payload = tokenpayload
    endPoint = baseUri : tokenApi
RETURN

BUILD.cashInPreAuthPayload:
    cashInPreAuthPayLoad = ""
    cashInPreAuthPayLoad = '{"clientIdentifier": {"type": "':tranData['~',1,1]:'","value": "':tranData['~',2,1]:'"},'
    cashInPreAuthPayLoad := '"amount":{"value": ':amount:',"currency": "':tranData['~',4,1]:'"}}'
            
    payload = cashInPreAuthPayLoad
    endPoint = baseUri : cashInPreAuthApi
RETURN

BUILD.cashOutPreAuthPayload:
    cashOutPreAuthPayload = ""
    cashOutPreAuthPayload = '{"clientIdentifier": {"type": "':tranData['~',1,1]:'","value": "':tranData['~',2,1]:'"},'
    cashOutPreAuthPayload := '"amount":{"value": ':amount:',"currency": "':tranData['~',4,1]:'"}}'
            
    payload = cashOutPreAuthPayload
    endPoint = baseUri : cashOutPreAuthApi
RETURN

BUILD.authenticatePinAcceptorPayLoad:
    authenticatePinAcceptorPayLoad = ""
    authenticatePinAcceptorPayLoad := '{"pincode": "':pinAcceptorpin:'"}'
    
    payload = authenticatePinAcceptorPayLoad
    CHANGE "transactionToken4" TO token IN authPinAcceptor
    endPoint = baseUri : authPinAcceptor
RETURN
   
BUILD.cashInMakePayLoad:
    cashInMakePayLoad = "{}"
    payload = cashInMakePayLoad
    
    CHANGE "transactionToken4" TO token IN cashInMakeApi
    endPoint = baseUri : cashInMakeApi
RETURN

BUILD.cashOutMakePayLoad:
    cashOutMakePayLoad = "{}"
    payload = cashOutMakePayLoad
    
    CHANGE "transactionToken4" TO token IN cashOutMakeApi
    endPoint = baseUri : cashOutMakeApi
RETURN

BUILD.authenticateSendOTPPayLoad:
    
    authenticateSendOTPPayLoad = "{}"
    payload = authenticateSendOTPPayLoad
    CHANGE "transactionToken4" TO token IN authSendOtp
    endPoint = baseUri : authSendOtp
RETURN

BUILD.authenticateVerifyOTPPayLoad:
    authenticateVerifyOTPPayLoad = '{"otp": "':otp:'"}'
    payload = authenticateVerifyOTPPayLoad
    CHANGE "transactionToken4" TO token IN authVerifyOtp
    endPoint = baseUri : authVerifyOtp
RETURN

PARSE.payload:
    IF NOT(error) THEN
        value = SFF.Util.ParsePayLoadResponse(response, field)
    END
    
RETURN
END
