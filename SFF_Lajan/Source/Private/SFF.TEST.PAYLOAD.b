* @ValidationCode : MjotOTAzMTc4OTk6Q3AxMjUyOjE2MDAzODg5ODc4NTg6a3VkemFuYWkubWFzaXdhOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjE5X1NQMTIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 18 Sep 2020 02:29:47
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : kudzanai.masiwa
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP12.0
$PACKAGE SFF.Lajan
SUBROUTINE SFF.TEST.PAYLOAD
*-----------------------------------------------------------------------------
*
*-----------------------------------------------------------------------------
* Modification History :
*-----------------------------------------------------------------------------

*-----------------------------------------------------------------------------
    $USING EB.SystemTables
    clientIdentifierType = "ID"
    clientIdentifierValue = "1421"
    amount = "100"
    description = "TEST"
    tranType = "CASHIN"
    tranStatus = "PREAUTH"
    transactionData = "PHONE~50937027447~100~HTG"

    SFF.Lajan.LajanPayloadRequest(clientIdentifierType, clientIdentifierValue, amount, description, tranType, tranStatus, transactionData, transDataResp, error)
    CRT "###############PREAUTH###################################"
    CRT transDataResp
    CRT "##################################################"
    token = transDataResp["~",1,1]
    transactionData = token
    tranStatus = "AUTH"
    SFF.Lajan.LajanPayloadRequest(clientIdentifierType, clientIdentifierValue, amount, description, tranType, tranStatus, transactionData, transDataResp, error)
    CRT "##############AUTH####################################"
    CRT transDataResp
    token = transDataResp["~",1,1]
    CRT "##################################################"
    tranStatus = "COMPLETE"
    SFF.Lajan.LajanPayloadRequest(clientIdentifierType, clientIdentifierValue, amount, description, tranType, tranStatus, transactionData, transDataResp, error)
    CRT "##############COMPLETE####################################"
    CRT transDataResp
    CRT "##################################################"
    INPUT F
END
