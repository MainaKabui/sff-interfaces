* @ValidationCode : MjoxNTE3ODQ4NjE4OkNwMTI1MjoxNjAxMDI2MjYzOTkyOmt1ZHphbmFpLm1hc2l3YTotMTotMTowOjA6ZmFsc2U6Ti9BOlIxOV9TUDEyLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 25 Sep 2020 11:31:03
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : kudzanai.masiwa
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP12.0
$PACKAGE SFF.Lajan
SUBROUTINE V.SFF.LAJAN.SEND.AUTH.RTN
    
*-----------------------------------------------------------------------------
*
*-----------------------------------------------------------------------------
* Modification History :
*-----------------------------------------------------------------------------

*-----------------------------------------------------------------------------
    $USING EB.SystemTables
    $USING TT.Contract
    $USING EB.LocalReferences
    $USING FT.Contract
    
    GOSUB Init
    
RETURN

Init:
  
    tranType =  EB.SystemTables.getRNew(SFF.Lajan.LajanCash.TransType)
    abortStatus = EB.SystemTables.getRNew(SFF.Lajan.LajanCash.Abort)
    rNw = ""
    IF abortStatus EQ "YES" THEN RETURN
    
    message = EB.SystemTables.getMessage()
    sep = "~"
    
    BEGIN CASE
        CASE tranType EQ "CASHIN"
            GOSUB GET.TransDetails
            GOSUB PROCESS.Transaction
        CASE tranType EQ "CASHOUT"
            GOSUB GET.TransDetails
            GOSUB PROCESS.Transaction
        CASE tranType EQ "BILL.PAYMENT"
        CASE tranType EQ "REVERSAL"
        CASE 1
        
    END CASE
    
    
    IF error THEN
        EB.SystemTables.setE(error)
    END
    
    
RETURN


GET.TransDetails:
    appl = EB.SystemTables.getApplication()
    BEGIN CASE
        CASE appl EQ "TELLER"
            
            EB.LocalReferences.GetLocRef(appl, "UNIQUE.EXT.REF", pos)
            walletId = EB.SystemTables.getRNew(TT.Contract.Teller.TeLocalRef)<1,pos>
            walletRec = SFF.Lajan.LajanCash.Read(walletId, Error)
            IF Error THEN
                EB.SystemTables.setE("Missing Record ":walletId)
                RETURN
            END ELSE
                GOSUB GET.WalletDetails
            END

        CASE appl EQ "FUNDS.TRANSFER"
        
            EB.LocalReferences.GetLocRef(appl, "UNIQUE.EXT.REF", pos)
            walletId = EB.SystemTables.getRNew(FT.Contract.FundsTransfer.LocalRef)<1,pos>
            walletRec = SFF.Lajan.LajanCash.Read(walletId, Error)
            IF Error THEN
                EB.SystemTables.setE("Missing Record ":walletId)
                RETURN
            END ELSE
                GOSUB GET.WalletDetails
            END

        CASE 1
           
            clientIdentifierType = EB.SystemTables.getRNew(SFF.Lajan.LajanCash.RequestKey)
            clientIdentifierValue = EB.SystemTables.getRNew(SFF.Lajan.LajanCash.RequestValue)
            amount = EB.SystemTables.getRNew(SFF.Lajan.LajanCash.B2cAmount)
            narrative = EB.SystemTables.getRNew(SFF.Lajan.LajanCash.B2cDescription)
            tranType = EB.SystemTables.getRNew(SFF.Lajan.LajanCash.TransType)
            tranStatus = EB.SystemTables.getRNew(SFF.Lajan.LajanCash.TransStatus)
            sessionNo = EB.SystemTables.getRNew(SFF.Lajan.LajanCash.SessionNo)
            otp = EB.SystemTables.getRNew(SFF.Lajan.LajanCash.B2cOtp)
            IF NOT(sessionNo) THEN
                sessionNo = EB.SystemTables.getROld(SFF.Lajan.LajanCash.SessionNo)
            END
        
    END CASE
    

    
    BEGIN CASE
        CASE tranStatus EQ 'PREAUTH'
            transactionData = clientIdentifierType :sep: clientIdentifierValue :sep: amount :sep: "HTG"
        CASE tranStatus EQ 'AUTH'
            transactionData = sessionNo:sep:clientIdentifierType :sep: clientIdentifierValue :sep: amount :sep: "HTG":sep:otp
        CASE tranStatus EQ 'COMPLETE'
            transactionData = sessionNo:sep:clientIdentifierType :sep: clientIdentifierValue :sep: amount :sep: "HTG":sep:otp
        CASE 1
    END CASE
RETURN

PROCESS.Transaction:
    IF message EQ 'AUT' THEN
        SFF.Lajan.LajanPayloadRequest(clientIdentifierType, clientIdentifierValue, amount, narrative, tranType, tranStatus, transactionData, transDataResp, error)
        IF NOT(sessionNo) AND rNw EQ "" THEN 
          EB.SystemTables.setRNew(SFF.Lajan.LajanCash.SessionNo, transDataResp[sep,1,1])
        END
        
        IF NOT (sessionNo) AND rNw EQ "true" THEN 
          walletRec<SFF.Lajan.LajanCash.TransStatus> = "COMPLETE"
        END
    END
RETURN

GET.WalletDetails:
    tranStatus = "COMPLETE"
    rNw = "true"
    clientIdentifierType = walletRec<SFF.Lajan.LajanCash.RequestKey>
    clientIdentifierValue = walletRec<SFF.Lajan.LajanCash.RequestValue>
    amount = walletRec<SFF.Lajan.LajanCash.B2cAmount>
    narrative = walletRec<SFF.Lajan.LajanCash.B2cDescription>
    tranType = walletRec<SFF.Lajan.LajanCash.TransType>
    tranStatus = walletRec<SFF.Lajan.LajanCash.TransStatus>
    sessionNo = walletRec<SFF.Lajan.LajanCash.SessionNo>
RETURN


END
