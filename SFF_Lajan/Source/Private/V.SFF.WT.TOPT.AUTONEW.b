* @ValidationCode : MjoxOTk0NjI5MzUwOkNwMTI1MjoxNjAwMjE1NjI5ODE1Omt1ZHphbmFpLm1hc2l3YTotMTotMTowOjA6ZmFsc2U6Ti9BOlIxOV9TUDYuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 16 Sep 2020 02:20:29
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : kudzanai.masiwa
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP6.0
$PACKAGE SFF.Lajan
SUBROUTINE V.SFF.WT.TOPT.AUTONEW
    $USING EB.SystemTables
*-----------------------------------------------------------------------------
*
*-----------------------------------------------------------------------------
* Modification History :
*-----------------------------------------------------------------------------

*-----------------------------------------------------------------------------

    GOSUB Init
    GOSUB Process

Init:
    TranType = EB.SystemTables.getRNew(SFF.Lajan.LajanCash.TransType)
RETURN

Process:
    tmpT = EB.SystemTables.getT(SFF.Lajan.LajanCash.TransType)
    tmpN = EB.SystemTables.getN(SFF.Lajan.LajanCash.TransType)
    IF TranType EQ "CASHOUT" THEN
        tmpT<3> = ""
        tmpN = "36.1.C"
    END ELSE
        tmpT<3> = "NOINPUT"
        tmpN = "36..C"
    END
    tmpT = EB.SystemTables.setT(SFF.Lajan.LajanCash.TransType,tmpT)
    tmpN = EB.SystemTables.setN(SFF.Lajan.LajanCash.TransType,tmpN)
RETURN

END
