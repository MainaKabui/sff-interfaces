* @ValidationCode : MjotMzU4MjQ4MTUzOkNwMTI1MjoxNjExNjUyNzUwNDI4Omt1ZHphbmFpLm1hc2l3YTotMTotMTowOjA6ZmFsc2U6Ti9BOlIxOV9TUDEyLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 26 Jan 2021 11:19:10
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : kudzanai.masiwa
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP12.0
$PACKAGE SFF.Lajan
SUBROUTINE SFF.Lajan.Acct.Check.Nofile(enqData)
    $USING EB.Reports
    $USING EB.DataAccess
    $USING AC.AccountOpening
    $USING AC.API
    $USING EB.API
    $USING EB.SystemTables
    $USING AA.Accounting
    $USING SFF.Util
*-----------------------------------------------------------------------------
*
*-----------------------------------------------------------------------------
* Modification History :
*-----------------------------------------------------------------------------

*-----------------------------------------------------------------------------
    GOSUB Initialize
    GOSUB Process
RETURN

Initialize:

    position1 = ""
    LOCATE "ACCT.NO" IN EB.Reports.getDFields()<1> SETTING position1 THEN
        AcctId = EB.Reports.getDRangeAndValue()<position1>
    END

    position1 = ""
    LOCATE "PURPOSE" IN EB.Reports.getDFields()<1> SETTING position1 THEN
        ActPurp = EB.Reports.getDRangeAndValue()<position1>
    END
    
    Sep = "*"
    MaturityDate = ""
    NextRepayment = ""
    DuePayment = ""
    Balance = ""
RETURN

Process:
    
    accountRecord = AC.AccountOpening.Account.Read(AcctId, AcErr)

        
    BEGIN CASE
        CASE ActPurp EQ "ACCOUNT.CREDIT"
            IF NOT(accountRecord) THEN
                
                EB.Reports.setEnqError("Account - ":AcctId:" - does not exist")
            END ELSE
                acctName = accountRecord(AC.AccountOpening.Account.ShortTitle)
            END
            GOSUB GET.AccountCredit
        CASE ActPurp EQ "LOAN.REPAYMENT"
            GOSUB GET.LoanRepayment
        CASE ActPurp EQ "DEPOSIT.TOPUP"
            GOSUB GET.DepositTopup
        CASE 1
    END CASE
   
RETURN

GET.AccountCredit:
    AcctRec = ""
    BalanceType = "BOOKING"
    BalanceDate = EB.SystemTables.getToday()
    SystemDate = ""
    Balance = ""
    CreditMvmt = ""
    DebitMvmt = ""
    ErrMsg = ""
    
    AC.API.EbGetAcctBalance(AcctId, AcctRec, BalanceType, BalanceDate, SystemDate, Balance, CreditMvmt, DebitMvmt, ErrMsg)
    GOSUB BUILD.EnquiryArray
RETURN

GET.LoanRepayment:
    
* Replace Test Data with actual data
*    GOSUB GET.ArrangementId
*    SFF.Util.GetAALoanNextInstalments(arrangementId, repaymentAmount, repaymentDate)
    repaymentAmount = "145.00"
    tDay = EB.SystemTables.getToday()
    MaturityDate = EB.SystemTables.getToday()
    EB.API.Cdt('', tDay, '+10C')
    repaymentDate = tDay
    EB.API.Cdt('', MaturityDate, '+30C')
    DuePayment = "160.00"
    Balance = "1500.00"
    acctName = "John Doe"
    GOSUB BUILD.EnquiryArray
RETURN

GET.DepositTopup:
*    GOSUB GET.ArrangementId
* Replace Test Data with actual data
    tDay = EB.SystemTables.getToday()
    MaturityDate = EB.SystemTables.getToday()
    EB.API.Cdt('', MaturityDate, '+60C')
    DuePayment = "0.00"
    Balance = "3000.00"
    acctName = "Jane Doe"
    GOSUB BUILD.EnquiryArray
RETURN

BUILD.EnquiryArray:
    NextRepaymentAmount = repaymentAmount
    NextRepaymentDate = repaymentDate
    Balance = FMT(Balance,"R2#15")
    enqData<-1> = acctName:Sep:Balance:Sep:DuePayment:Sep:NextRepaymentAmount:Sep:NextRepaymentDate:Sep:MaturityDate
RETURN

GET.ArrangementId:
    arrId = accountRecord<AC.AccountOpening.Account.ArrangementId>
RETURN

END