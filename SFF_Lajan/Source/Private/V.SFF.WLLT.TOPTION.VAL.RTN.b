* @ValidationCode : MjoxOTc3ODg3ODQ0OkNwMTI1MjoxNjAwNDMzODQwNDc4Omt1ZHphbmFpLm1hc2l3YTotMTotMTowOjA6ZmFsc2U6Ti9BOlIxOV9TUDEyLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 18 Sep 2020 14:57:20
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : kudzanai.masiwa
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP12.0
$PACKAGE SFF.Lajan
SUBROUTINE V.SFF.WLLT.TOPTION.VAL.RTN
*-----------------------------------------------------------------------------
*
*-----------------------------------------------------------------------------
* Modification History :
*-----------------------------------------------------------------------------

*-----------------------------------------------------------------------------
    $USING EB.SystemTables
    $USING EB.Versions
    
    GOSUB Init
    GOSUB Process
    
RETURN

Init:
    transferOption = EB.SystemTables.getComi()
    tranType = EB.SystemTables.getRNew(SFF.Lajan.LajanCash.TransType)
    trfOptC = "TRANSFER.OPTION"
    B2cOtpC = "B2C.OTP"
RETURN

Process:
    tmpT = EB.SystemTables.getT(SFF.Lajan.LajanCash.AccountNo)
    rVerTmp = EB.SystemTables.getRVersion(EB.Versions.Version.VerMandatoryField)


    IF transferOption EQ "ACCOUNT" THEN
        tmpT<3> = ""
        GOSUB CheckAddTrnOptMandatory
    END ELSE
        tmpT<3> = "NOINPUT"
        GOSUB CheckRemTrnOptMandatory
    END
    EB.SystemTables.setT(SFF.Lajan.LajanCash.AccountNo, tmpT)
   
    tmpTotp = EB.SystemTables.getT(SFF.Lajan.LajanCash.B2cOtp)
    BEGIN CASE
        CASE tranType EQ "CASHOUT"
            tmpTotp<3> = ""
            GOSUB CheckAddOtpMandatory
        CASE 1
            tmpTotp<3> = "NOINPUT"
            GOSUB CheckRemOtptMandatory
    END CASE
    EB.SystemTables.setT(SFF.Lajan.LajanCash.B2cOtp, tmpTotp)
    
RETURN

CheckAddTrnOptMandatory:
    FINDSTR trfOptC IN rVerTmp SETTING Ap, Vp ELSE
        rVerTmp = rVerTmp:@VM:trfOptC
        EB.SystemTables.setRVersion(EB.Versions.Version.VerMandatoryField, rVerTmp)
    END
RETURN

CheckAddOtpMandatory:
    FINDSTR B2cOtpC IN rVerTmp SETTING Ap, Vp ELSE
        rVerTmp = rVerTmp:@VM:B2cOtpC
        EB.SystemTables.setRVersion(EB.Versions.Version.VerMandatoryField, rVerTmp)
    END
RETURN

CheckRemTrnOptMandatory:
    FINDSTR trfOptC IN rVerTmp SETTING Ap, Vp THEN
        DEL rVerTmp<Vp>
        EB.SystemTables.setRVersion(EB.Versions.Version.VerMandatoryField, rVerTmp)
    END
RETURN

CheckRemOtptMandatory:
    FINDSTR B2cOtpC IN rVerTmp SETTING Ap, Vp THEN
        DEL rVerTmp<Vp>
        EB.SystemTables.setRVersion(EB.Versions.Version.VerMandatoryField, rVerTmp)
    END
RETURN
END
