package com.sg.sff;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.lang3.StringEscapeUtils;

import com.temenos.tafj.common.jSession;
import com.temenos.tafj.common.jVar;
import com.temenos.tafj.runtime.jRunTime;
import com.temenos.tafj.runtime.extension.BasicReplacement;

public class SFFGenericListener extends BasicReplacement {

    private static final String className = SFFGenericListener.class.getName();

    @Override
    public jVar invoke(Object... arg0) {
        String request = String.valueOf(arg0[0]);
        String result = processRequest(request);
        ((jVar) arg0[1]).set(result);
        return null;
    }

    private String processRequest(final String data) {
        String[] arr1 = data.split("\\|");
        Map<String, String> map = new HashMap<>();
        if (arr1.length > 0)
            for (String s : arr1) {
                String[] arr2 = s.split("\\*");
                if (arr2.length == 2)
                    map.put(arr2[0], arr2[1]);
            }

        if (map.size() == 0)
            return "05|Invalid number of parameter";

        String result = "";
        Object obj = map.get("requestType");
        if (obj == null)
            return "05|Invalid request type";

        String requestType = obj.toString();
        switch (requestType) {
        case "getISODate":
            result = getISODate();
            break;
        case "escapeString":
            result = escapeString(map);
            break;
        case "getMonCashData":
            result = getMonCashData(map);
            break;
        case "postRequest":
            result = postRequest(map);
            break;
        default:
            return "05|Unsupported request type";
        }

        return result;
    }

    private String getISODate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        return sdf.format(new Date());
    }

    private String escapeString(final Map<String, String> map) {
        String str = map.get("payload");
        return StringEscapeUtils.escapeJava(str);
    }

    private String getMonCashData(final Map<String, String> map) {
        String statusCode = "05";
        String statusMsg = "";
        String obj = null;
        try {
            obj = map.get("password");
            if (obj == null)
                throw new Exception("Password not set");
            String password = obj;

            obj = map.get("dateFormat");
            if (obj == null)
                throw new Exception("Date Format not set");
            String dateFormat = obj;

            obj = map.get("timeZone");
            if (obj == null)
                throw new Exception("Timezone not set");
            String timeZone = obj;

            SecureRandom rand = SecureRandom.getInstance("SHA1PRNG");
            rand.setSeed(System.currentTimeMillis());
            byte[] nonceBytes = new byte[16];
            rand.nextBytes(nonceBytes);

            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
            sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
            String created = sdf.format(new Date());
            byte[] createdDateBytes = created.getBytes("UTF-8");

            byte[] passwordBytes = password.getBytes("UTF-8");

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            baos.write(nonceBytes);
            baos.write(createdDateBytes);
            baos.write(passwordBytes);

            MessageDigest md = MessageDigest.getInstance("SHA-1");
            byte[] digestedPassword = md.digest(baos.toByteArray());

            String passwordDigest = base64(digestedPassword);
            String baseNoonce = base64(nonceBytes);
            statusCode = "00";
            statusMsg = passwordDigest.concat("|").concat(baseNoonce).concat("|").concat(created);
        } catch (NoSuchAlgorithmException ex) {
            statusMsg = String.format("%s:%s", "NoSuchAlgorithmException: ", ex.getMessage());
        } catch (IOException ex) {
            statusMsg = String.format("%s:%s", "NoSuchAlgorithmException: ", ex.getMessage());
        } catch (Exception ex) {
            statusMsg = String.format("%s:%s", "NoSuchAlgorithmException: ", ex.getMessage());
        }

        String result = String.format("%s|%s", statusCode, statusMsg);

        return result;
    }

    private String base64(final byte[] bytes) {
        return Base64.getEncoder().encodeToString(bytes);
    }

    private synchronized String postRequest(final Map<String, String> map) {
        String statusCode = "05";
        String statusMsg = "Generic Error 01";
        String obj = null;

        URL url;
        HttpURLConnection con = null;
        InputStream in = null;
        BufferedReader bf = null;
        try {
            allowCerts();

            obj = map.get("endPoint");
            if (obj == null)
                throw new Exception("EndPoint not set");
            String endPoint = obj;

            obj = map.get("connectionTimeout");
            if (obj == null)
                throw new Exception("Connection Timeout not set");
            int connectionTimeout = Integer.parseInt(obj);

            obj = map.get("sessionTimeout");
            if (obj == null)
                throw new Exception("Session Timeout not set");
            int sessionTimeout = Integer.parseInt(obj);

            obj = map.get("headers");
            if (obj == null)
                throw new Exception("HTTP Headers not set");
            String headers = obj;

            obj = map.get("payload");
            if (obj == null)
                throw new Exception("Request Payload not set");
            String payload = obj;

            System.out.println(String.format("The request is: %s", payload));

            url = new URL(endPoint);
            con = endPoint.startsWith("http") ? (HttpURLConnection) url.openConnection()
                    : (HttpsURLConnection) url.openConnection();
            con.setRequestMethod("POST");

            String[] arr = headers.split("\\^");
            if (arr.length > 0)
                for (String s : arr) {
                    String[] arr2 = s.split("\\:");
                    if (arr2.length > 0)
                        con.setRequestProperty(arr2[0], arr2[1]);
                }

            con.setConnectTimeout(connectionTimeout);
            con.setReadTimeout(sessionTimeout);
            con.setDoOutput(true);

            DataOutputStream dos = new DataOutputStream(con.getOutputStream());
            dos.write(payload.getBytes());
            dos.flush();
            dos.close();

            in = con.getInputStream();
            if (in == null) {
                in = con.getErrorStream();
            }

            bf = new BufferedReader(new InputStreamReader(in));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bf.readLine()) != null) {
                sb.append(line);
            }

            statusCode = "00";
            statusMsg = sb.toString();
        } catch (MalformedURLException ex) {
            statusMsg = String.format("%s:%s", "MalformedURLException: ", ex.getMessage());
        } catch (IOException ex) {
            statusMsg = String.format("%s:%s", "IOException: ", ex.getMessage());
        } catch (NoSuchAlgorithmException ex) {
            statusMsg = String.format("%s:%s", "NoSuchAlgorithmException: ", ex.getMessage());
        } catch (KeyManagementException ex) {
            statusMsg = String.format("%s:%s", "KeyManagementException: ", ex.getMessage());
        } catch (Exception ex) {
            statusMsg = String.format("%s:%s", "Exception: ", ex.getMessage());
        } finally {
            if (bf != null) {
                try {
                    bf.close();
                } catch (Exception ex) {
                    ex.toString();
                }
            }

            if (in != null) {
                try {
                    in.close();
                } catch (Exception ex) {
                    ex.toString();
                }
            }

            if (con != null) {
                try {
                    con.disconnect();
                } catch (Exception ex) {
                    ex.toString();
                }
            }
        }

        String result = String.format("%s|%s", statusCode, statusMsg);
        System.out.println(String.format("The result is: %s", statusMsg));

        return result;
    }

    private synchronized void allowCerts() throws NoSuchAlgorithmException, KeyManagementException {
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        } };

        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
    }

    public static jRunTime INSTANCE(jSession session) {
        jRunTime prg = session.getRuntimeCache(className);
        if (prg == null) {
            prg = new SFFGenericListener();
            prg.init(session);
        }
        return prg;
    }

    public void stack(jRunTime prg) {
        session.setRuntimeCache(className, prg);
    }

}
