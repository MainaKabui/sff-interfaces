* @ValidationCode : MjoyMDQ0Njk2Nzc3OkNwMTI1MjoxNTk5NzM2ODAwOTQ4Ok1haW5hOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjE5X1NQMjQuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 10 Sep 2020 14:20:00
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MGram
*
* Implementation of SFF.MGram.CustValidationRtn
*
*
SUBROUTINE V.SFF.MG.CUST.VALIDATION.RTN
    $USING EB.SystemTables
    $USING ST.Customer

    GOSUB Init
RETURN

Init:
    customerId = EB.SystemTables.getComi()
    IF NOT(customerId) THEN
        RETURN
    END
    
    GOSUB GetCustomerDetails
RETURN

GetCustomerDetails:
    recCus = ST.Customer.Customer.CacheRead(customerId, error)
    senderFirstName = recCus<ST.Customer.Customer.EbCusGivenNames>
    senderLastName = recCus<ST.Customer.Customer.EbCusFamilyName>
    senderDOB = recCus<ST.Customer.Customer.EbCusDateOfBirth>
    senderIdType = "INT"
    senderIdNumber = recCus<ST.Customer.Customer.EbCusLegalId>
    senderOccupation = recCus<ST.Customer.Customer.EbCusOccupation>
    senderPhotoType = recCus<ST.Customer.Customer.EbCusLegalDocName>
    IF LEFT(senderPhotoType,8) EQ "PASSPORT" THEN
        senderPhotoType = "PAS"
    END ELSE
        senderPhotoType = "GOV"
    END
    senderPhotoId = senderIdNumber
    senderPhotoCountry = recCus<ST.Customer.Customer.EbCusNationality>
    senderNationality = recCus<ST.Customer.Customer.EbCusNationality>
    senderAddress = recCus<ST.Customer.Customer.EbCusAddress>
    senderCity = recCus<ST.Customer.Customer.EbCusTownCountry>
    
    GOSUB UpdateDetails
RETURN

UpdateDetails:
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.SenderFirstName, senderFirstName)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.SenderLastName, senderLastName)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.SenderDob, senderDOB)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.SenderLegalIdType, senderIdType)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.SenderLegalIdNumber, senderIdNumber)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.SenderOccupation, senderOccupation)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.SenderPhotoIdType, senderPhotoType)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.SenderPhotoIdNumber, senderPhotoId)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.SenderPhotoIdCountry, senderPhotoCountry)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.SenderCitizenshipCountry, senderNationality)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.SenderAddress, senderAddress)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.SenderCity, senderCity)
RETURN

END