* @ValidationCode : MjotMTE5MzIwMjE3OkNwMTI1MjoxNTk5NzM5MTAxOTg4Ok1haW5hOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjE5X1NQMjQuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 10 Sep 2020 14:58:21
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MGram
*
* Implementation of SFF.MGram.ReceiveAuthRtn
*
*
SUBROUTINE V.SFF.MG.RECEIVE.AUTH.RTN
    $USING EB.SystemTables
    $USING EB.ErrorProcessing
    
    GOSUB Init
RETURN
    
Init:
    referenceNumber = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReferenceNumber)
    receiveCurrency = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiveCurrency)
    agentCheckNumber = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.AgentCheckNumber)
    agentCheckAmount = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.AgentCheckAmount)
    receiverAddress = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverAddress)
    receiverCity = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverCity)
    receiverCountry = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverCountry)
    receiverPhotoIdType = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverPhotoIdType)
    receiverPhotoIdNumber = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverPhotoIdNumber)
    receiverPhotoIdCountry = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverPhotoIdCountry)
    receiverLegalIdType = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverLegalIdType)
    receiverLegalIdNumber = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverLegalIdNumber)
    receiverDoB = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverDob)
    receiverPhoneCountryCode = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverPhoneCountryCode)
    receiverPhone = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverPhone)
    relationshipToSender = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.RelationshipToSender)
    receiverCitizenshipCountry = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverCitizenshipCountry)
    sessionId = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.SessionId)
    
    GOSUB Process
RETURN

Process:
    CHANGE '.' TO '_' IN receiverPhotoIdType
    CHANGE '.' TO '_' IN receiverLegalIdType
    CHANGE '.' TO '_' IN relationshipToSender
    
    receiverCitizenshipCountry = SFF.MGram.GetCountryIsoCode(receiverCitizenshipCountry)
    receiverCountry = SFF.MGram.GetCountryIsoCode(receiverCountry)
    receiverPhotoIdCountry = SFF.MGram.GetCountryIsoCode(receiverPhotoIdCountry)
    receiverDoB = LEFT(receiverDoB, 4):'-':receiverDoB[5,2]:'-':RIGHT(receiverDoB,2)
        
    SFF.MGram.ReceiveValidation(referenceNumber, receiveCurrency, agentCheckNumber, agentCheckAmount, receiverAddress, receiverCity, receiverCountry, receiverPhotoIdType, receiverPhotoIdNumber, receiverPhotoIdCountry, receiverLegalIdType, receiverLegalIdNumber, receiverDoB, receiverPhoneCountryCode, receiverPhone, relationshipToSender, receiverCitizenshipCountry, sessionId, error)
    IF error THEN
        EB.SystemTables.setE(error)
        EB.SystemTables.setAf("")
        RETURN
    END
RETURN

END