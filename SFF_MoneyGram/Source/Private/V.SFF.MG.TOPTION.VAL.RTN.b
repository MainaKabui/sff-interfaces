* @ValidationCode : MjotMTQ5MzM4MDY3MDpDcDEyNTI6MTU5OTczNTU2MTM5NzpNYWluYTotMTotMTowOjA6ZmFsc2U6Ti9BOlIxOV9TUDI0LjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 10 Sep 2020 13:59:21
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MGram
*
* Implementation of SFF.MGram.TransferOptValidationRtn
*
SUBROUTINE V.SFF.MG.TOPTION.VAL.RTN
    $USING EB.SystemTables
    $USING EB.Display
    $USING EB.Versions

    GOSUB Init
RETURN

Init:
    transferOption = EB.SystemTables.getComi()
    
    GOSUB Process
RETURN

Process:
    BEGIN CASE
        CASE NOT(transferOption)
            GOSUB DeactivateAccountNo
            GOSUB DeactivateMSISDN
        CASE transferOption EQ "CASH"
            GOSUB DeactivateAccountNo
            GOSUB DeactivateMSISDN
        CASE transferOption EQ "MONCASH"
            GOSUB ActivateMSISDN
            GOSUB DeactivateAccountNo
        CASE 1
            GOSUB ActivateAccountNo
            GOSUB DeactivateMSISDN
    END CASE
RETURN

ActivateMSISDN:
    tmpT = EB.SystemTables.getT(SFF.MGram.MoneyGram.Msisdn)
    tmpT<3> = ""
    EB.SystemTables.setT(SFF.MGram.MoneyGram.Msisdn, tmpT)

    tmpN = "20.1.C"
    EB.SystemTables.setN(SFF.MGram.MoneyGram.Msisdn, tmpN)
RETURN

DeactivateMSISDN:
    tmpT = EB.SystemTables.getT(SFF.MGram.MoneyGram.Msisdn)
    tmpT<3> = "NOINPUT"
    EB.SystemTables.setT(SFF.MGram.MoneyGram.Msisdn, tmpT)

    tmpN = "20..C"
    EB.SystemTables.setN(SFF.MGram.MoneyGram.Msisdn, tmpN)
RETURN

ActivateAccountNo:
    tmpT = EB.SystemTables.getT(SFF.MGram.MoneyGram.AccountNo)
    tmpT<3> = ""
    EB.SystemTables.setT(SFF.MGram.MoneyGram.AccountNo, tmpT)

    tmpN = "36.1.C"
    EB.SystemTables.setN(SFF.MGram.MoneyGram.AccountNo, tmpN)
RETURN

DeactivateAccountNo:
    tmpT = EB.SystemTables.getT(SFF.MGram.MoneyGram.AccountNo)
    tmpT<3> = "NOINPUT"
    EB.SystemTables.setT(SFF.MGram.MoneyGram.AccountNo, tmpT)
    
    tmpN = "36..C"
    EB.SystemTables.setN(SFF.MGram.MoneyGram.AccountNo, tmpN)
RETURN

END