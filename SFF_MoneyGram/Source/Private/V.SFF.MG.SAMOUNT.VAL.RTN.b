* @ValidationCode : MjozNzQzNDYwOTU6Q3AxMjUyOjE1OTY0NTgyNzA5NTQ6TWFpbmE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMTlfU1AyNC4wOi0xOi0x
* @ValidationInfo : Timestamp         : 03 Aug 2020 15:37:50
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MGram
*
* Implementation of SFF.MGram.SendAmountValidation
*
*
SUBROUTINE V.SFF.MG.SAMOUNT.VAL.RTN
    $USING EB.SystemTables
    $USING EB.ErrorProcessing
    $USING EB.LocalReferences
    $USING ST.Config

    GOSUB Init
RETURN

Init:      
    amount = EB.SystemTables.getComi()
    countryCode = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverCountry)
    sendCurrency = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.SendCurrency)
    productType = "SEND"
    
    GOSUB GetFields
RETURN

GetFields:
    receiveCountry = SFF.MGram.GetCountryIsoCode(countryCode)
    SFF.MGram.Feelookup(receiveCountry, sendCurrency, amount, productType, sendAmount, receiveCurrency, receiveAmount, validExchangeRate, totalSendFees, totalSendTaxes, sessionId, error)
    IF error THEN
        EB.SystemTables.setEtext(error)
        EB.ErrorProcessing.StoreEndError()
        EB.SystemTables.setAf("")
        RETURN
    END
    
    GOSUB UpdateFields
RETURN

UpdateFields:
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.ReceiveCurrency, receiveCurrency)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.ReceiveAmount, receiveAmount)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.SendAmount, sendAmount)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.SendFees, totalSendFees)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.SendTaxes, totalSendTaxes)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.ReceiveRate, validExchangeRate)
    SFF.MGram.setSessionID(sessionId)
RETURN

END