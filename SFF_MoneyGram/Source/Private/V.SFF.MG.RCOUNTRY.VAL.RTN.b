* @ValidationCode : Mjo0ODM3NTkzMTY6Q3AxMjUyOjE1OTYwNTcwMTQ1MzI6TWFpbmE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMTlfU1AyNC4wOi0xOi0x
* @ValidationInfo : Timestamp         : 30 Jul 2020 00:10:14
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MGram

SUBROUTINE V.SFF.MG.RCOUNTRY.VAL.RTN
    $USING EB.SystemTables
    
    GOSUB Init
RETURN

Init:
    countryCode = EB.SystemTables.getComi()    
    GOSUB GetCurrency
RETURN

GetCurrency:
    BEGIN CASE
        CASE countryCode EQ "HT"
            sendCurrency = "HTG"
        CASE 1
            sendCurrency = "USD"
    END CASE
    
    GOSUB UpdateFields
RETURN

UpdateFields:
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.SendCurrency, sendCurrency)
RETURN
  
END
