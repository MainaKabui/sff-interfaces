* @ValidationCode : MjotNjQyODgzMDkyOkNwMTI1MjoxNTk5Nzc1Njg4NDk5Ok1haW5hOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjE5X1NQMjQuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 11 Sep 2020 01:08:08
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MGram
*
* Implementation of SFF.MGram.SendTxnCheckRtn
*
*
SUBROUTINE V.SFF.MG.SEND.TXN.CHECK.RTN
    $USING EB.SystemTables
    $USING PW.Foundation
    $USING TT.Contract
    $USING FT.Contract
    $USING EB.ErrorProcessing

    GOSUB Init
RETURN

Init:
    appln = EB.SystemTables.getApplication()
    IF appln EQ "TELLER" THEN
        GOSUB SetTTtxnCode
    END ELSE
        GOSUB SetFTtxnCode
    END
RETURN

SetTTtxnCode:
    ttTxn = SFF.MGram.GetCommon("SEND.TT.TRANSACTION")
    IF NOT(ttTxn) THEN
        EB.SystemTables.setE("SEND.TT.TRANSACTION")
        RETURN
    END
    
    EB.SystemTables.setRNew(TT.Contract.Teller.TeTransactionCode, ttTxn)
    
    GOSUB SetTTCrAcct
RETURN

SetTTCrAcct:
    ccy = EB.SystemTables.getRNew(TT.Contract.Teller.TeCurrencyOne)
    creditAcctNo = SFF.MGram.GetCommon(ccy:".COLLECTION.ACCT")
    IF NOT(creditAcctNo) THEN
        EB.SystemTables.setE("Missing the ":ccy:" COLLECTION.ACCT")
        RETURN
    END
    
    EB.SystemTables.setRNew(TT.Contract.Teller.TeAccountTwo, creditAcctNo)
RETURN

SetFTtxnCode:
    ftTxn = SFF.MGram.GetCommon("FT.TRANSACTION")
    IF NOT(ftTxn) THEN
        EB.SystemTables.setE("Missing FT.TRANSACTION")
        RETURN
    END
    
    EB.SystemTables.setRNew(FT.Contract.FundsTransfer.TransactionType, ftTxn)
    
    GOSUB SetFTCrAcct
RETURN

SetFTCrAcct:
    ccy = EB.SystemTables.getRNew(FT.Contract.FundsTransfer.CreditCurrency)
    creditAcctNo = SFF.MGram.GetCommon(ccy:".COLLECTION.ACCT")
    IF NOT(creditAcctNo) THEN
        EB.SystemTables.setE("Missing the ":ccy:" COLLECTION.ACCT")
        RETURN
    END
    
    EB.SystemTables.setRNew(FT.Contract.FundsTransfer.CreditAcctNo, creditAcctNo)
RETURN

END