* @ValidationCode : MjotMjA1ODAzMTgwOkNwMTI1MjoxNTk5NzQxMDA2MzUyOk1haW5hOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjE5X1NQMjQuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 10 Sep 2020 15:30:06
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MGram
*
* Implementation of SFF.MGram.ReceiveAcctValidationRtn
*
*
SUBROUTINE V.SFF.MG.RECEIVE.ACCT.VAL.RTN
    $USING EB.SystemTables
    $USING AC.AccountOpening
    $USING EB.ErrorProcessing
   
    GOSUB Init
RETURN
    
Init:
    accountNumber = EB.SystemTables.getComi()
    IF NOT(accountNumber) THEN
        RETURN
    END
    
    receiveOption = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiveOption)
               
    GOSUB GetAccountRec
RETURN

GetAccountRec:
    rec = AC.AccountOpening.Account.Read(accountNumber, error)
    IF error THEN
        EB.SystemTables.setEtext(error)
        EB.ErrorProcessing.StoreEndError()
        EB.SystemTables.setAf("")
        RETURN
    END
    acctCateg = rec<AC.AccountOpening.Account.Category>
    
    GOSUB ValidateAcctCategory
RETURN

ValidateAcctCategory:
    BEGIN CASE
        CASE receiveOption EQ "ACCOUNT"
            sendCategories = SFF.MGram.GetCommon("RECEIVE.CATEGORY")
        CASE receiveOption EQ "LOAN"
            sendCategories = SFF.MGram.GetCommon("LOANS.CATEGORY")
        CASE receiveOption EQ "DEPOSIT"
            sendCategories = SFF.MGram.GetCommon("DEPOSITS.CATEGORY")
    END CASE
        
    j = DCOUNT(sendCategories, @SM)
    found = ""
    FOR i = 1 TO j
        categRange = FIELD(sendCategories, @SM, i)
        fromCateg = FIELD(categRange, " ", 1)
        toCateg = FIELD(categRange, " ", 2)
        IF acctCateg GE fromCateg AND acctCateg LE toCateg THEN
            found = 1
            BREAK
        END
    NEXT i
    
    IF NOT(found) THEN
        EB.SystemTables.setEtext("Invalid Account Category")
        EB.ErrorProcessing.StoreEndError()
        EB.SystemTables.setAf("")
        RETURN
    END
RETURN

END
