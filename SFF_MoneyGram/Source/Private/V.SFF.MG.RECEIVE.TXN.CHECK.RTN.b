* @ValidationCode : MjotNTg4ODM4OTg1OkNwMTI1MjoxNTk5NzY5ODgzNzEwOk1haW5hOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjE5X1NQMjQuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 10 Sep 2020 23:31:23
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MGram
*
* Implementation of SFF.MGram.ReceiveTxnCheckRtn
*
*
SUBROUTINE V.SFF.MG.RECEIVE.TXN.CHECK.RTN
    $USING EB.SystemTables
    $USING PW.Foundation
    $USING TT.Contract
    $USING FT.Contract
    $USING EB.ErrorProcessing

    GOSUB Init
RETURN

Init:
    appln = EB.SystemTables.getApplication()
    IF appln EQ "TELLER" THEN
        GOSUB SetTTtxnCode
    END ELSE
        GOSUB SetFTtxnCode
    END
RETURN

SetTTtxnCode:
    ttTxn = SFF.MGram.GetCommon("RECEIVE.TT.TRANSACTION")
    IF NOT(ttTxn) THEN
        EB.SystemTables.setE("Missing RECEIVE.TT.TRANSACTION")
        RETURN
    END
    
    EB.SystemTables.setRNew(TT.Contract.Teller.TeTransactionCode, ttTxn)
    
    GOSUB SetTTDrAcct
RETURN

SetTTDrAcct:
    ccy = EB.SystemTables.getRNew(TT.Contract.Teller.TeCurrencyOne)
    debitAcctNo = SFF.MGram.GetCommon(ccy:".COLLECTION.ACCT")
    IF NOT(debitAcctNo) THEN
        EB.SystemTables.setE(ccy:".COLLECTION.ACCT")
        RETURN
    END
    
    EB.SystemTables.setRNew(TT.Contract.Teller.TeAccountTwo, debitAcctNo)
RETURN

SetFTtxnCode:
    fttc = EB.SystemTables.getRNew(FT.Contract.FundsTransfer.TransactionType)
    IF NOT(fttc) THEN
        ftTxn = SFF.MGram.GetCommon("FT.TRANSACTION")
        IF NOT(ftTxn) THEN
            EB.SystemTables.setE("Missing FT.TRANSACTION")
            RETURN
        END
        EB.SystemTables.setRNew(FT.Contract.FundsTransfer.TransactionType, ftTxn)
    END

    GOSUB SetFTDrAcct
RETURN

SetFTDrAcct:
    ccy = EB.SystemTables.getRNew(FT.Contract.FundsTransfer.DebitCurrency)
    debitAcctNo = SFF.MGram.GetCommon(ccy:".COLLECTION.ACCT")
    IF NOT(debitAcctNo) THEN
        EB.SystemTables.setE("Missing the ":ccy:" COLLECTION.ACCT")
        RETURN
    END
    
    EB.SystemTables.setRNew(FT.Contract.FundsTransfer.DebitAcctNo, debitAcctNo)
    
    GOSUB SetOrderingBank
RETURN

SetOrderingBank:
    EB.SystemTables.setRNew(FT.Contract.FundsTransfer.OrderingBank, SFF.MGram.BankName)
RETURN

END