* @ValidationCode : MjotMTk4Nzg0NDgzNzpDcDEyNTI6MTYwMDExMDQ3NTk1NjpNYWluYTotMTotMTowOjA6ZmFsc2U6Ti9BOlIxOV9TUDI0LjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 14 Sep 2020 22:07:55
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MGram
*
* Implementation of SFF.MGram.SendQuoteCheckRtn
*
*
SUBROUTINE V.SFF.MG.SEND.QUOTE.CHECK.RTN
    $USING EB.SystemTables
    $USING EB.ErrorProcessing
    $USING ST.ExchangeRate
    $USING ST.CurrencyConfig
    
    

    GOSUB Init
RETURN

Init:
    countryCode = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverCountry)
    transferCurrency = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.TransferCurrency)
    transferAmount = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.TransferAmount)
    totalAmountToCollect = transferAmount
    receiveAmount = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiveAmount)
    productType = "SEND"
    
    GOSUB SetSendCurrency
RETURN

SetSendCurrency:
    IF countryCode EQ "HT" OR transferCurrency EQ "HTG" THEN
        sendCurrency = "HTG"
    END ELSE
        sendCurrency = "USD"
    END
    
    GOSUB ExecuteFeelookup
RETURN

ExecuteFeelookup:
    receiveCountry = SFF.MGram.GetCountryIsoCode(countryCode)
    SFF.MGram.Feelookup(receiveCountry, sendCurrency, totalAmountToCollect, productType, sendAmount, receiveCurrency, receiveAmount, validExchangeRate, totalSendFees, totalSendTaxes, sessionId, error)
    IF error THEN
        EB.SystemTables.setE(error)
        RETURN
    END

    GOSUB GetTotalAmountToCollect
RETURN

GetTotalAmountToCollect:
    IF NOT(transferAmount) THEN
        ccyMkt = SFF.MGram.GetCommon("CCY.MARKET")
        custRate = ""
        ccyBuy = transferCurrency
        buyAmt = ""
        ccySell = sendCurrency
        sellAmt = totalAmountToCollect
        returnCode = ""
        ST.ExchangeRate.Custrate(ccyMkt, ccyBuy, buyAmt, ccySell, sellAmt, "", "", custRate, "", "", "", "", returnCode)
        error = EB.SystemTables.getEtext()
        IF error THEN
            EB.SystemTables.setE(error)
            RETURN
        END
        totalAmountToCollect = buyAmt
    END
    
    GOSUB UpdateFields
RETURN
    
UpdateFields:
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.ReceiveCurrency, receiveCurrency)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.ReceiveAmount, receiveAmount)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.TransferAmount, totalAmountToCollect)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.SendAmount, sendAmount)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.SendCurrency, sendCurrency)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.SendFees, totalSendFees)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.SendTaxes, totalSendTaxes)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.ReceiveRate, DROUND(validExchangeRate,2))
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.SessionId, sessionId)
RETURN

END