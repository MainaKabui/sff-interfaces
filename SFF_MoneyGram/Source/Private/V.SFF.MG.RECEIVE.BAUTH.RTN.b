* @ValidationCode : Mjo3NjIzMDc2NjQ6Q3AxMjUyOjE1OTgwNTA1MDkzMDQ6TWFpbmE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMTlfU1AyNC4wOi0xOi0x
* @ValidationInfo : Timestamp         : 22 Aug 2020 01:55:09
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MGram
*
* Implementation of SFF.MGram.ReceiveBAuthRtn
*
*
SUBROUTINE V.SFF.MG.RECEIVE.BAUTH.RTN
    $USING EB.SystemTables
    $USING EB.Foundation
    $USING TT.Contract
    $USING FT.Contract
    $USING EB.Interface
    
    GOSUB Init
RETURN
    
Init:
    receiveCurrency = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiveCurrency)
    transferOption = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.TransferOption)
    debitAmount =  EB.SystemTables.getRNew(SFF.MGram.MoneyGram.AgentCheckAmount)
    ofsId = SFF.MGram.GetCommon("OFS.ID")
    IF NOT(ofsId) THEN
        EB.SystemTables.setE("Missing OFS.ID")
        RETURN
    END
    
    category = SFF.MGram.GetCommon("CASH.CATEGORY")
    IF NOT(category) THEN
        EB.SystemTables.setE("Missing the Cash Category")
        RETURN
    END
    
    GOSUB GetDebitAccount
RETURN

GetDebitAccount:
    debitAccount = SFF.MGram.GetCommon(receiveCurrency:".COLLECTION.ACCT")
    IF NOT(debitAccount) THEN
        EB.SystemTables.setE("Missing MoneyGram's Collection Account")
        RETURN
    END
    
    GOSUB GetCreditAccount
RETURN

GetCreditAccount:
    IF transferOption EQ "CASH" THEN
        transactionType = SFF.MGram.GetCommon("TT.TRANSACTION")
        currentUser = EB.SystemTables.getOperator()
        rec = TT.Contract.TellerUser.CacheRead(currentUser, err)
        IF err THEN
            EB.SystemTables.setE("Sorry you do not have a Till")
            RETURN
        END
    
        tellerId = rec<1>
        creditAccount = receiveCurrency:category:tellerId
                                      
        appName = "TELLER"
        versionName = "TELLER,SFF.MONEYGRAM"
        transRec<TT.Contract.Teller.TeTransactionCode> = transactionType
        transRec<TT.Contract.Teller.TeAccountOne> = debitAccount
        IF receiveCurrency EQ EB.SystemTables.getLccy() THEN
            transRec<TT.Contract.Teller.TeAmountLocalOne> = debitAmount
        END ELSE
            transRec<TT.Contract.Teller.TeAmountFcyOne> = debitAmount
        END
        transRec<TT.Contract.Teller.TeTellerIdTwo> = tellerId
        transRec<TT.Contract.Teller.TeCurrencyOne> = receiveCurrency
        transRec<TT.Contract.Teller.TeAccountTwo> = creditAccount
    END ELSE
        appName = "FUNDS.TRANSFER"
        versionName = "FUNDS.TRANSFER,SFF.MONEYGRAM"
        creditAccount = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.AccountNo)
        transactionType = SFF.MGram.GetCommon("FT.TRANSACTION")
        transRec<FT.Contract.FundsTransfer.TransactionType> = transactionType
        transRec<FT.Contract.FundsTransfer.DebitAcctNo> = debitAccount
        transRec<FT.Contract.FundsTransfer.DebitAmount> = debitAmount
        transRec<FT.Contract.FundsTransfer.DebitCurrency> = receiveCurrency
        transRec<FT.Contract.FundsTransfer.CreditAcctNo> = creditAccount
        transRec<FT.Contract.FundsTransfer.OrderingBank> = SFF.MGram.BankName
    END
    
    GOSUB BuildOfs
RETURN

BuildOfs:
    ofsFunc = "I"
    process = "PROCESS"
    gtsMode = ""
    noOfAuth = 0
    EB.Foundation.OfsBuildRecord(appName, ofsFunc, process, versionName, gtsMode, noOfAuth, transactionId, transRec, ofsReq)

    insertOrAdd = 'ADD'
    EB.Interface.OfsAddlocalrequest(ofsReq, insertOrAdd, ofsError)
    IF ofsError THEN
        EB.SystemTables.setE(ofsError)
        RETURN
    END
                     
    GOSUB SaveSession
RETURN

SaveSession:
    SFF.MGram.setMoneyGramId(EB.SystemTables.getIdNew())
    SFF.MGram.setProductType("RCV")
RETURN

END
