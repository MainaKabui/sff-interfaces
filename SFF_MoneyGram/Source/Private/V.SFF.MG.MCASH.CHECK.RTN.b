* @ValidationCode : MjotMTQ1MzU1NzIxMTpDcDEyNTI6MTU5OTc2OTcwMzYzNTpNYWluYTotMTotMTowOjA6ZmFsc2U6Ti9BOlIxOV9TUDI0LjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 10 Sep 2020 23:28:23
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MGram
*
* Implementation of SFF.MGram.ReceiveMonCashCheckRtn
*
*
SUBROUTINE V.SFF.MG.MCASH.CHECK.RTN
    $USING EB.SystemTables
    $USING SFF.MonCash
    $USING FT.Contract
    
    GOSUB SetFTCrAcct
RETURN

SetFTCrAcct:
    creditAcctNo = SFF.MonCash.GetCommon("COLLECTION.ACCT")
    IF NOT(creditAcctNo) THEN
        EB.SystemTables.setE("Missing the COLLECTION.ACCT")
        RETURN
    END
    
    EB.SystemTables.setRNew(FT.Contract.FundsTransfer.CreditAcctNo, creditAcctNo)
RETURN

END