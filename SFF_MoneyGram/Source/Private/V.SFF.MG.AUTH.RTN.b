* @ValidationCode : MjotMTgwOTI5MDU2MTpDcDEyNTI6MTU5OTc1MjkyNjM1MjpNYWluYTotMTotMTowOjA6ZmFsc2U6Ti9BOlIxOV9TUDI0LjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 10 Sep 2020 18:48:46
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0

$PACKAGE SFF.MGram
*
* Implementation of SFF.MGram.AuthRtn
*
*
SUBROUTINE V.SFF.MG.AUTH.RTN
    $USING PW.Foundation
    $USING EB.SystemTables
    $USING PW.API
    $USING SFF.Util
    $USING SFF.MonCash
    $USING FT.Contract
    $USING EB.Foundation
    $USING EB.Interface
    
    GOSUB Init
RETURN
    
Init:
    t24Ref = EB.SystemTables.getIdNew()
    activityId = PW.Foundation.getActivityTxnId()
                   
    GOSUB GetMoneyGramData
RETURN

GetMoneyGramData:
    IF NOT(activityId) THEN
        EB.SystemTables.setE("Invalid Activity Id")
        EB.SystemTables.setAf("")
        RETURN
    END
                     
    moneyGramId = SFF.Util.GetPwTransactionId(activityId, error)
    IF error THEN
        EB.SystemTables.setE(error)
        EB.SystemTables.setAf("")
        RETURN
    END
    
    rec = SFF.MGram.MoneyGram.Read(moneyGramId, error)
    IF error THEN
        EB.SystemTables.setE(error)
        EB.SystemTables.setAf("")
        RETURN
    END
    
    sessionId = rec<SFF.MGram.MoneyGram.SessionId>
    productType = rec<SFF.MGram.MoneyGram.SendReceive>
        
    GOSUB CommitTxn
RETURN

CommitTxn:
    SFF.MGram.CommitTransaction(sessionId, productType, referenceNo, deliveryDate, error)
    IF error THEN
        EB.SystemTables.setE(error)
        EB.SystemTables.setAf("")
        RETURN
    END
    
    GOSUB MonCashTransfer
RETURN

MonCashTransfer:
    receiveOption = rec<SFF.MGram.MoneyGram.ReceiveOption>
    IF NOT(productType EQ "RCV" AND receiveOption EQ "MONCASH") THEN
        GOSUB UpdateFields
        RETURN
    END
    
    msisdn = rec<SFF.MGram.MoneyGram.Msisdn>
    amount = EB.SystemTables.getRNew(FT.Contract.FundsTransfer.DebitAmount)
    debitAccount = EB.SystemTables.getRNew(FT.Contract.FundsTransfer.DebitAcctNo)
    SFF.MonCash.MTransfer(moneyGramId, msisdn, amount, description, transId, error)
    IF error THEN
        EB.SystemTables.setE(error)
        EB.SystemTables.setAf("")
        RETURN
    END
    
    referenceNo := @SM:transId
    
    GOSUB UpdateMonCash
RETURN

UpdateMonCash:
    appName = "EB.SFF.MONCASH"
    versionName = "EB.SFF.MONCASH,SEND.OFS"
    transRec<SFF.MonCash.MonCash.B2CTo> = msisdn
    transRec<SFF.MonCash.MonCash.B2CAmount> = amount
    transRec<SFF.MonCash.MonCash.TransferOption> = "ACCOUNT"
    transRec<SFF.MonCash.MonCash.AccountNo> = debitAccount
    transRec<SFF.MonCash.MonCash.SendReceive> = "SEND"
    transRec<SFF.MonCash.MonCash.Abort> = "NO"
    transRec<SFF.MonCash.MonCash.T24TxnRef> = t24Ref
    
    ofsFunc = "I"
    process = "PROCESS"
    gtsMode = ""
    noOfAuth = 0
    EB.Foundation.OfsBuildRecord(appName, ofsFunc, process, versionName, gtsMode, noOfAuth, transactionId, transRec, ofsReq)
        
    ofsId = SFF.MGram.GetCommon("OFS.ID")
    ofsSourceRec = EB.Interface.OfsSource.Read(ofsId, error)
    EB.Interface.setOfsSourceId(ofsId)
    EB.Interface.setOfsSourceRec(ofsSourceRec)
    EB.Interface.OfsProcessManager(ofsReq, ofsRes)
    
    GOSUB UpdateFields
RETURN

UpdateFields:
    rec<SFF.MGram.MoneyGram.T24TxnRef> = t24Ref
    rec<SFF.MGram.MoneyGram.ReferenceNumber> = referenceNo
    rec<SFF.MGram.MoneyGram.DeliveryDate> = deliveryDate
    SFF.MGram.MoneyGram.Write(moneyGramId, rec)
RETURN

END