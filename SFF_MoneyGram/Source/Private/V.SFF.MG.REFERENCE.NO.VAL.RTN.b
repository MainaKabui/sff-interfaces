* @ValidationCode : MjotMTQzOTMyODQ2MzpDcDEyNTI6MTU5OTYzNjE2ODgyOTpNYWluYTotMTotMTowOjA6ZmFsc2U6Ti9BOlIxOV9TUDI0LjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 09 Sep 2020 10:22:48
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MGram
*
* Implementation of SFF.MGram.RefValidationRtn
*
*
SUBROUTINE V.SFF.MG.REFERENCE.NO.VAL.RTN
    $USING EB.SystemTables
    $USING EB.ErrorProcessing
    
    GOSUB Init
RETURN

Init:
    referenceNumber = EB.SystemTables.getComi()
    
    GOSUB GetDetails
RETURN

GetDetails:
    SFF.MGram.ReferenceNumber(referenceNumber, senderFirstName, senderLastName, senderHomePhone, receiverFirstName, receiverLastName, receiveCurrency, agentCheckNumber, agentCheckAmount, sessionId, error)
    IF error THEN
        EB.SystemTables.setEtext(error)
        EB.ErrorProcessing.StoreEndError()
        RETURN
    END
        
    GOSUB UpdateFields
RETURN

UpdateFields:
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.SenderFirstName, senderFirstName)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.SenderLastName, senderLastName)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.SenderHomePhone, senderHomePhone)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.ReceiverFirstName, receiverFirstName)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.ReceiverLastName, receiverLastName)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.ReceiveCurrency, receiveCurrency)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.AgentCheckNumber, agentCheckNumber)
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.AgentCheckAmount, DROUND(agentCheckAmount,2))
    EB.SystemTables.setRNew(SFF.MGram.MoneyGram.SessionId, sessionId)
RETURN

END