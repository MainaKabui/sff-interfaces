* @ValidationCode : MjotMTU3NTY5NTA4OTpDcDEyNTI6MTU5OTc0MDQyMDE1ODpNYWluYTotMTotMTowOjA6ZmFsc2U6Ti9BOlIxOV9TUDI0LjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 10 Sep 2020 15:20:20
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MGram
*
* Implementation of SFF.MGram.SendAcctValidationRtn
*
*
SUBROUTINE V.SFF.MG.SEND.ACCT.VAL.RTN
    $USING EB.SystemTables
    $USING AC.AccountOpening
    $USING EB.ErrorProcessing
    
    GOSUB Init
RETURN
    
Init:
    accountNumber = EB.SystemTables.getComi()
    IF NOT(accountNumber) THEN
        RETURN
    END
    
    customerCode = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.CustomerCode)
    transferCurrency = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.TransferCurrency)
           
    GOSUB GetAccountRec
RETURN

GetAccountRec:
    rec = AC.AccountOpening.Account.Read(accountNumber, error)
    IF error THEN
        EB.SystemTables.setEtext(error)
        EB.ErrorProcessing.StoreEndError()
        EB.SystemTables.setAf("")
        RETURN
    END
    acctCust = rec<AC.AccountOpening.Account.Customer>
    acctCcy = rec<AC.AccountOpening.Account.Currency>
    acctCateg = rec<AC.AccountOpening.Account.Category>

    GOSUB ValidateCustomer
RETURN

ValidateCustomer:
    IF customerCode AND customerCode NE acctCust THEN
        EB.SystemTables.setEtext("The account does not belong to the customer")
        EB.ErrorProcessing.StoreEndError()
        EB.SystemTables.setAf("")
        RETURN
    END
    
    GOSUB ValidateAcctCcy
RETURN
    
ValidateAcctCcy:
    IF transferCurrency NE acctCcy THEN
        EB.SystemTables.setEtext("Account currency is not: ": transferCurrency)
        EB.ErrorProcessing.StoreEndError()
        EB.SystemTables.setAf("")
        RETURN
    END
    
    GOSUB ValidateAcctCategory
RETURN

ValidateAcctCategory:
    sendCategories = SFF.MGram.GetCommon("SEND.CATEGORY")
    j = DCOUNT(sendCategories, @SM)
    found = ""
    FOR i = 1 TO j
        categRange = FIELD(sendCategories, @SM, i)
        fromCateg = FIELD(categRange, " ", 1)
        toCateg = FIELD(categRange, " ", 2)
        IF acctCateg GE fromCateg AND acctCateg LE toCateg THEN
            found = 1
            BREAK
        END
    NEXT i
    
    IF NOT(found) THEN
        EB.SystemTables.setEtext("Invalid Account Category")
        EB.ErrorProcessing.StoreEndError()
        EB.SystemTables.setAf("")
        RETURN
    END
RETURN

END