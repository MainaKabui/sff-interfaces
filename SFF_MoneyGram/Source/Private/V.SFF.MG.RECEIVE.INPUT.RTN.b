* @ValidationCode : Mjo4MTU5NTQ3NjM6Q3AxMjUyOjE1OTY0ODc1ODc4OTE6TWFpbmE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMTlfU1AyNC4wOi0xOi0x
* @ValidationInfo : Timestamp         : 03 Aug 2020 23:46:27
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MGram
*
* Implementation of SFF.MGram.ReceiveInputRtn
*
*
SUBROUTINE V.SFF.MG.RECEIVE.INPUT.RTN
    $USING EB.SystemTables
    $USING EB.ErrorProcessing
    
    GOSUB Init
RETURN
    
Init:
    referenceNumber = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReferenceNumber)
    receiveCurrency = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiveCurrency)
    agentCheckNumber = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.AgentCheckNumber)
    agentCheckAmount = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.AgentCheckAmount)
    receiverAddress = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverAddress)
    receiverCity = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverCity)
    receiverCountry = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverCountry)
    receiverPhotoIdType = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverPhotoIdType)
    receiverPhotoIdNumber = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverPhotoIdNumber)
    receiverPhotoIdCountry = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverPhotoIdCountry)
    receiverLegalIdType = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverLegalIdType)
    receiverLegalIdNumber = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverLegalIdNumber)
    receiverDoB = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverDob)
    receiverPhoneCountryCode = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverPhoneCountryCode)
    receiverPhone = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverPhone)
    relationshipToSender = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.RelationshipToSender)
    receiverCitizenshipCountry = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverCitizenshipCountry)
    sessionId = SFF.MGram.getSessionID()
    
    GOSUB Process
RETURN

Process:
    receiverCitizenshipCountry = SFF.MGram.GetCountryIsoCode(receiverCitizenshipCountry)
    receiverCountry = SFF.MGram.GetCountryIsoCode(receiverCountry)
    receiverPhotoIdCountry = SFF.MGram.GetCountryIsoCode(receiverPhotoIdCountry)
    receiverDoB = LEFT(receiverDoB, 4):'-':receiverDoB[5,2]:'-':RIGHT(receiverDoB,2)
    
    SFF.MGram.ReceiveValidation(referenceNumber, receiveCurrency, agentCheckNumber, agentCheckAmount, receiverAddress, receiverCity, receiverCountry, receiverPhotoIdType, receiverPhotoIdNumber, receiverPhotoIdCountry, receiverLegalIdType, receiverLegalIdNumber, receiverDoB, receiverPhoneCountryCode, receiverPhone, relationshipToSender, receiverCitizenshipCountry, sessionId, error)
    IF error THEN
        EB.SystemTables.setEtext(error)
        EB.ErrorProcessing.StoreEndError()
        EB.SystemTables.setAf("")
        RETURN
    END
RETURN

END
