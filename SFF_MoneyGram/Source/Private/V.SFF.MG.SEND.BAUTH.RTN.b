* @ValidationCode : MjotMTQwOTIxMDYyODpDcDEyNTI6MTU5ODA1MDU3ODI5OTpNYWluYTotMTotMTowOjA6ZmFsc2U6Ti9BOlIxOV9TUDI0LjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 22 Aug 2020 01:56:18
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MGram
*
* Implementation of SFF.MGram.SendBAuthRtn
*
*
SUBROUTINE V.SFF.MG.SEND.BAUTH.RTN
    $USING EB.SystemTables
    $USING EB.Foundation
    $USING TT.Contract
    $USING FT.Contract
    $USING EB.Interface
    
    GOSUB Init
RETURN
    
Init:
    sendCurrency = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.SendCurrency)
    transferOption = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.TransferOption)
    debitAmount =  EB.SystemTables.getRNew(SFF.MGram.MoneyGram.TransferAmount)
    ofsId = SFF.MGram.GetCommon("OFS.ID")
    IF NOT(ofsId) THEN
        EB.SystemTables.setE("Missing OFS.ID")
        RETURN
    END
    
    category = SFF.MGram.GetCommon("CASH.CATEGORY")
    IF NOT(category) THEN
        EB.SystemTables.setE("Missing the Cash Category")
        RETURN
    END
    
    GOSUB GetCreditAccount
RETURN

GetCreditAccount:
    creditAccount = SFF.MGram.GetCommon(sendCurrency:".COLLECTION.ACCT")
    IF NOT(creditAccount) THEN
        EB.SystemTables.setE("Missing MoneyGram's Collection Account")
        RETURN
    END
    
    GOSUB GetDebitAccount
RETURN

GetDebitAccount:
    IF transferOption EQ "CASH" THEN
        transactionType = SFF.MGram.GetCommon("TT.TRANSACTION")
        currentUser = EB.SystemTables.getOperator()
        rec = TT.Contract.TellerUser.CacheRead(currentUser, err)
        IF err THEN
            EB.SystemTables.setE("Sorry you do not have a Till")
            RETURN
        END
    
        tellerId = rec<1>
        debitAccount = sendCurrency:category:tellerId
        
        appName = "TELLER"
        versionName = "TELLER,SFF.MONEYGRAM"
        transRec<TT.Contract.Teller.TeTransactionCode> = transactionType
        transRec<TT.Contract.Teller.TeTellerIdOne> = tellerId
        transRec<TT.Contract.Teller.TeAccountOne> = debitAccount
        IF sendCurrency EQ EB.SystemTables.getLccy() THEN
            transRec<TT.Contract.Teller.TeAmountLocalOne> = debitAmount
        END ELSE
            transRec<TT.Contract.Teller.TeAmountFcyOne> = debitAmount
        END
        transRec<TT.Contract.Teller.TeCurrencyOne> = sendCurrency
        transRec<TT.Contract.Teller.TeAccountTwo> = creditAccount
    END ELSE
        appName = "FUNDS.TRANSFER"
        versionName = "FUNDS.TRANSFER,SFF.MONEYGRAM"
        debitAccount = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.AccountNo)
        transactionType = SFF.MGram.GetCommon("FT.TRANSACTION")
        transRec<FT.Contract.FundsTransfer.TransactionType> = transactionType
        transRec<FT.Contract.FundsTransfer.DebitAcctNo> = debitAccount
        transRec<FT.Contract.FundsTransfer.DebitAmount> = debitAmount
        transRec<FT.Contract.FundsTransfer.DebitCurrency> = sendCurrency
        transRec<FT.Contract.FundsTransfer.CreditAcctNo> = creditAccount
        transRec<FT.Contract.FundsTransfer.OrderingBank> = SFF.MGram.BankName
    END
    
    GOSUB BuildOfs
RETURN

BuildOfs:
    ofsFunc = "I"
    process = "PROCESS"
    gtsMode = ""
    noOfAuth = 0
    EB.Foundation.OfsBuildRecord(appName, ofsFunc, process, versionName, gtsMode, noOfAuth, transactionId, transRec, ofsReq)
    
    insertOrAdd = 'ADD'
    EB.Interface.OfsAddlocalrequest(ofsReq, insertOrAdd, ofsError)
    IF ofsError THEN
        EB.SystemTables.setE(ofsError)
        RETURN
    END
                
    GOSUB SaveSession
RETURN

SaveSession:
    SFF.MGram.setMoneyGramId(EB.SystemTables.getIdNew())
    SFF.MGram.setProductType("SEND")
RETURN

END