* @ValidationCode : Mjo4MjEzNzQzOTI6Q3AxMjUyOjE1OTk2MzUzOTM1NDk6TWFpbmE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMTlfU1AyNC4wOi0xOi0x
* @ValidationInfo : Timestamp         : 09 Sep 2020 10:09:53
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MGram
*
* Implementation of SFF.MGram.TxnAuthRtn
*
*
SUBROUTINE V.SFF.MG.SEND.AUTH.RTN
    $USING EB.SystemTables
    $USING EB.ErrorProcessing
    
    GOSUB Init
RETURN
    
Init:
    senderFirstName = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.SenderFirstName)
    senderLastName = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.SenderLastName)
    senderDOB = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.SenderDob)
    senderLegalIdType = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.SenderLegalIdType)
    senderLegalIdNumber = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.SenderLegalIdNumber)
    senderOccupationOther = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.SenderOccupationOther)
    senderPhotoIdType = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.SenderPhotoIdType)
    senderPhotoIdNumber = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.SenderPhotoIdNumber)
    senderPhotoIdCountry = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.SenderPhotoIdCountry)
    senderCitizenshipCountry = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.SenderCitizenshipCountry)
    senderAddress = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.SenderAddress)
    senderCity = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.SenderCity)
    senderCountry = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.SenderCountry)
    sendCurrency = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.SendCurrency)
    amount = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.SendAmount)
    receiveCountry = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverCountry)
    deliveryOption = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.DeliveryOption)
    receiveCurrency= EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiveCurrency)
    phone = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.SenderHomePhone)
    countryCode = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.SenderHomePhoneCountryCode)
    receiverFirstName = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverFirstName)
    receiverLastName = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.ReceiverLastName)
    txnPurpose = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.SendPurposeOfTransaction)
    fundSource = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.SourceOfFunds)
    receiverRelation = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.RelationshipToReceiver)
    sessionId = EB.SystemTables.getRNew(SFF.MGram.MoneyGram.SessionId)
    
    GOSUB Process
RETURN

Process:
    CHANGE '.' TO '_' IN deliveryOption
    CHANGE '.' TO '_' IN txnPurpose
    CHANGE '.' TO '_' IN fundSource
    CHANGE '.' TO '_' IN receiverRelation
    
    senderCitizenshipCountry = SFF.MGram.GetCountryIsoCode(senderCitizenshipCountry)
    senderCountry = SFF.MGram.GetCountryIsoCode(senderCountry)
    senderPhotoIdCountry = SFF.MGram.GetCountryIsoCode(senderPhotoIdCountry)
    destinationCountry = SFF.MGram.GetCountryIsoCode(receiveCountry)
    senderDOB = LEFT(senderDOB, 4):'-':senderDOB[5,2]:'-':RIGHT(senderDOB,2)
    
    SFF.MGram.SendValidation(senderFirstName, senderLastName, senderDOB, senderLegalIdType, senderLegalIdNumber, senderOccupationOther, senderPhotoIdType, senderPhotoIdNumber, senderPhotoIdCountry, senderCitizenshipCountry, senderAddress, senderCity, senderCountry, sendCurrency, amount, destinationCountry, deliveryOption, receiveCurrency, phone, countryCode, receiverFirstName, receiverLastName, txnPurpose, fundSource, receiverRelation, sessionId, error)
    IF error THEN
        EB.SystemTables.setE(error)
        EB.SystemTables.setAf("")
        RETURN
    END
RETURN

END