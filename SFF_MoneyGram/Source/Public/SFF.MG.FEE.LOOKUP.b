* @ValidationCode : Mjo0OTA2MzQ5MjY6Q3AxMjUyOjE1OTk0NzMyMDUyMjM6TWFpbmE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMTlfU1AyNC4wOi0xOi0x
* @ValidationInfo : Timestamp         : 07 Sep 2020 13:06:45
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MGram
*
* Implementation of SFF.MGram.Feelookup
*
* country(IN) :
*
SUBROUTINE SFF.MG.FEE.LOOKUP(receiveCountry, sendCurrency, totalAmountToCollect, productType, sendAmount, receiveCurrency, receiveAmount, validExchangeRate, totalSendFees, totalSendTaxes, sessionId, error)
    $USING SFF.Util
   
    GOSUB Init
RETURN
    
Init:
    IF NOT(receiveCountry) THEN
        error = "Missing Receive Country"
        RETURN
    END
    
    IF NOT(sendCurrency) THEN
        error = "Missing Send Currency"
        RETURN
    END
    
    IF NOT(totalAmountToCollect) AND NOT(receiveAmount) THEN
        error = "Missing Amount"
        RETURN
    END
    
    IF NOT(productType) THEN
        error = "Missing Product Type"
        RETURN
    END
        
    endPoint = SFF.MGram.GetCommon("ENDPOINT")
    IF NOT(endPoint) THEN
        error = "Missing ENDPOINT"
        RETURN
    END
    
    connectionTimeout  = SFF.MGram.GetCommon("CONNECTION.TIMEOUT")
    IF NOT(connectionTimeout) THEN
        error = "Missing CONNECTION.TIMEOUT"
        RETURN
    END
    
    sessionTimeout  = SFF.MGram.GetCommon("SESSION.TIMEOUT")
    IF NOT(sessionTimeout) THEN
        error = "Missing SESSION.TIMEOUT"
        RETURN
    END
    
    agentID = SFF.MGram.GetCommon("AGENT.ID")
    IF NOT(agentID) THEN
        error = "Missing AGENT.ID"
        RETURN
    END
    
    agentSequence = SFF.MGram.GetCommon("AGENT.SEQUENCE")
    IF NOT(agentSequence) THEN
        error = "Missing AGENT.SEQUENCE"
        RETURN
    END
    
    token = SFF.MGram.GetCommon("TOKEN")
    IF NOT(token) THEN
        error = "Missing TOKEN"
        RETURN
    END
    
    apiVersion = SFF.MGram.GetCommon("API.VERSION")
    IF NOT(apiVersion) THEN
        error = "API.VERSION"
        RETURN
    END
    
    clientSoftwareVersion = SFF.MGram.GetCommon("CLIENT.SOFTWARE.VERSION")
    IF NOT(clientSoftwareVersion) THEN
        error = "CLIENT.SOFTWARE.VERSION"
        RETURN
    END
    
    channelType = SFF.MGram.GetCommon("CHANNEL.TYPE")
    IF NOT(channelType) THEN
        error = "CHANNEL.TYPE"
        RETURN
    END
    
    allOptions = SFF.MGram.GetCommon("ALL.OPTIONS")
    IF NOT(allOptions) THEN
        error = "ALL.OPTIONS"
        RETURN
    END

    timeStamp = ""
    
    GOSUB BuildHeaders
RETURN

BuildHeaders:
    headers = 'SOAPAction: urn:AgentConnect1512#feeLookup'
    headers := '^Content-Type: application/xml'
    GOSUB BuildPayload
RETURN

BuildPayload:
    request = 'requestType*getISODate'
    timeStamp = SFF.Util.InvokeEndPoint(request)
    IF NOT(timeStamp) THEN
        error = "Invalid Timestamp"
        RETURN
    END
               
    payload = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:agen="http://www.moneygram.com/AgentConnect1512">'
    payload := '<soapenv:Header/>'
    payload := '<soapenv:Body>'
    payload := '<agen:feeLookupRequest>'
    payload := '<agen:agentID>':agentID:'</agen:agentID>'
    payload := '<agen:agentSequence>':agentSequence:'</agen:agentSequence>'
    payload := '<agen:token>':token:'</agen:token>'
    payload := '<agen:timeStamp>':timeStamp:'</agen:timeStamp>'
    payload := '<agen:apiVersion>':apiVersion:'</agen:apiVersion>'
    payload := '<agen:clientSoftwareVersion>':clientSoftwareVersion:'</agen:clientSoftwareVersion>'
    payload := '<agen:channelType>':channelType:'</agen:channelType>'
    payload := '<agen:productType>':productType:'</agen:productType>'
    IF totalAmountToCollect THEN
        payload := '<agen:amountIncludingFee>':totalAmountToCollect:'</agen:amountIncludingFee>'
    END ELSE
        payload := '<agen:receiveAmount>':receiveAmount:'</agen:receiveAmount>'
    END
    payload := '<agen:receiveCountry>':receiveCountry:'</agen:receiveCountry>'
    payload := '<agen:sendCurrency>':sendCurrency:'</agen:sendCurrency>'
    payload := '<agen:allOptions>':allOptions:'</agen:allOptions>'
    payload := '</agen:feeLookupRequest>'
    payload := '</soapenv:Body>'
    payload := '</soapenv:Envelope>'
    
    GOSUB SendRequest
RETURN

SendRequest:
    request = 'endPoint*':endPoint
    request := '|connectionTimeout*':connectionTimeout
    request := '|sessionTimeout*':sessionTimeout
    request := '|headers*':headers
    request := '|payload*':payload
    request := '|requestType*postRequest'
   
    response = SFF.Util.InvokeEndPoint(request)
    statusCode = LEFT(response,2)
    statusMessage = RIGHT(response, LEN(response)-3)
    IF statusCode NE "00" THEN
        error = statusMessage
        RETURN
    END
      
    statusCode = "05"
    IF INDEX(statusMessage,"<ac:errorString>",1) THEN
        str = "<ac:errorString>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</ac:errorString>",1)
        c = b - a
        error = statusMessage[a,c]
        RETURN
    END
    
    GOSUB GetSendAmount
RETURN

GetSendAmount:
    IF INDEX(statusMessage,"<ac:sendAmount>",1) THEN
        str = "<ac:sendAmount>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</ac:sendAmount>",1)
        c = b - a
        sendAmount = statusMessage[a,c]
    END
    
    GOSUB GetTotalAmountToCollect
RETURN

GetTotalAmountToCollect:
    IF NOT(totalAmountToCollect) THEN
         IF INDEX(statusMessage,"<ac:totalAmountToCollect>",1) THEN
            str = "<ac:totalAmountToCollect>"
            a = INDEX(statusMessage,str,1) + LEN(str)
            b = INDEX(statusMessage,"</ac:totalAmountToCollect>",1)
            c = b - a
            totalAmountToCollect = statusMessage[a,c]
        END
    END
    
    GOSUB GetReceiveCurrency
RETURN

GetReceiveCurrency:
    IF INDEX(statusMessage,"<ac:payoutCurrency>",1) THEN
        str = "<ac:payoutCurrency>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</ac:payoutCurrency>",1)
        c = b - a
        receiveCurrency = statusMessage[a,c]
    END
    
    GOSUB GetReceiveAmount
RETURN
        
GetReceiveAmount:
    IF NOT(receiveAmount) THEN
        IF INDEX(statusMessage,"<ac:receiveAmount>",1) THEN
            str = "<ac:receiveAmount>"
            a = INDEX(statusMessage,str,1) + LEN(str)
            b = INDEX(statusMessage,"</ac:receiveAmount>",1)
            c = b - a
            receiveAmount = statusMessage[a,c]
        END
    END
    
    GOSUB GetValidExchange
RETURN
    
GetValidExchange:
    IF INDEX(statusMessage,"<ac:validExchangeRate>",1) THEN
        str = "<ac:validExchangeRate>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</ac:validExchangeRate>",1)
        c = b - a
        validExchangeRate = statusMessage[a,c]
    END
    
    GOSUB GetTotalFees
RETURN
    
GetTotalFees:
    IF INDEX(statusMessage,"<ac:totalSendFees>",1) THEN
        str = "<ac:totalSendFees>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</ac:totalSendFees>",1)
        c = b - a
        totalSendFees = statusMessage[a,c]
    END
    
    GOSUB GetTotalTaxes
RETURN
        
GetTotalTaxes:
    IF INDEX(statusMessage,"<ac:totalSendTaxes>",1) THEN
        str = "<ac:totalSendTaxes>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</ac:totalSendTaxes>",1)
        c = b - a
        totalSendTaxes = statusMessage[a,c]
    END
    
    GOSUB GetSessionId
RETURN

GetSessionId:
    IF INDEX(statusMessage,"<ac:mgiTransactionSessionID>",1) THEN
        str = "<ac:mgiTransactionSessionID>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</ac:mgiTransactionSessionID>",1)
        c = b - a
        sessionId = statusMessage[a,c]
    END
    
RETURN
    
END