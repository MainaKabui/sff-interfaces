* @ValidationCode : MjotMTY1NjczNzI1MzpDcDEyNTI6MTU5Nzc5MjQyODAyMzpNYWluYTotMTotMTowOjA6ZmFsc2U6Ti9BOlIxOV9TUDI0LjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 19 Aug 2020 02:13:48
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MGram
*
* Implementation of SFF.MGram.SendValidation
*
* amount(IN) :
* destinationCountry(IN) :
* deliveryOption(IN) :
* receiveCurrency(IN) :
* customer(IN) :
* phone(IN) :
* countryCode(IN) :
*
SUBROUTINE SFF.MG.SEND.VALIDATION(senderFirstName, senderLastName, senderDOB, senderLegalIdType, senderLegalIdNumber, senderOccupationOther, senderPhotoIdType, senderPhotoIdNumber, senderPhotoIdCountry, senderCitizenshipCountry, senderAddress, senderCity, senderCountry, sendCurrency, amount, destinationCountry, deliveryOption, receiveCurrency, phone, countryCode, receiverFirstName, receiverLastName, txnPurpose, fundSource, receiverRelation, sessionId, error)
    $USING SFF.Util
    
    GOSUB Init
RETURN
    
Init:
    IF NOT(senderFirstName) THEN
        error = "Missing Sender First Name"
        RETURN
    END
     
    IF NOT(senderLastName ) THEN
        error = "Missing Sender Last Name"
        RETURN
    END

    IF NOT(senderDOB) THEN
        error = "Missing Sender DOB"
        RETURN
    END

    IF NOT(senderLegalIdType ) THEN
        error = "Missing Legal Id Type"
        RETURN
    END
    
    IF NOT(senderLegalIdNumber) THEN
        error = "Missing Legal Id Number Type"
        RETURN
    END
    
    IF NOT(senderOccupationOther) THEN
        error = "Missing Sender Occupation"
        RETURN
    END
    
    IF NOT(senderPhotoIdType) THEN
        error = "Missing Sender Photo Id Type"
        RETURN
    END
    
    IF NOT(senderPhotoIdNumber) THEN
        error = "Missing Sender Photo Id Number"
        RETURN
    END
    
    IF NOT(senderPhotoIdCountry) THEN
        error = "Missing Sender Photo Id Country"
        RETURN
    END
    
    IF NOT(senderCitizenshipCountry) THEN
        error = "Missing Sender Nationality"
        RETURN
    END
    
    IF NOT(senderAddress) THEN
        error = "Missing Sender Address"
        RETURN
    END
    
    IF NOT(senderCity) THEN
        error = "Missing Sender City"
        RETURN
    END
    
    IF NOT(senderCountry) THEN
        error = "Missing Sender Country"
        RETURN
    END
    
    IF NOT(sendCurrency) THEN
        error = "Missing Sender Currency"
        RETURN
    END

    IF NOT(amount) THEN
        error = "Missing Transfer Amount"
        RETURN
    END
    
    IF NOT(destinationCountry) THEN
        error = "Missing Receive Country"
        RETURN
    END
    
    IF NOT(deliveryOption) THEN
        error = "Missing Receive Option"
        RETURN
    END
    
    IF NOT(receiveCurrency) THEN
        error = "Missing Receive Currency"
        RETURN
    END
           
    IF NOT(phone) THEN
        error = "Missing Sender's Phone"
        RETURN
    END
              
    IF NOT(countryCode) THEN
        error = "Missing Sender's Phone Country Code"
        RETURN
    END
    
    IF NOT(receiverFirstName) THEN
        error = "Missing Receiver First Name"
        RETURN
    END
    
    IF NOT(receiverLastName) THEN
        error = "Missing Receiver Last Name"
        RETURN
    END
    
    IF NOT(txnPurpose) THEN
        error = "Missing Transaction Purpose"
        RETURN
    END
    
    IF NOT(fundSource) THEN
        error = "Missing Source of Funds"
        RETURN
    END
    
    IF NOT(receiverRelation) THEN
        error = "Missing Relation To Receiver"
        RETURN
    END
    
    IF NOT(sessionId) THEN
        error = "Missing the Session Id"
        RETURN
    END
         
    endPoint = SFF.MGram.GetCommon("ENDPOINT")
    IF NOT(endPoint) THEN
        error = "Missing ENDPOINT"
        RETURN
    END
    
    connectionTimeout  = SFF.MGram.GetCommon("CONNECTION.TIMEOUT")
    IF NOT(connectionTimeout) THEN
        error = "Missing CONNECTION.TIMEOUT"
        RETURN
    END
    
    sessionTimeout  = SFF.MGram.GetCommon("SESSION.TIMEOUT")
    IF NOT(sessionTimeout) THEN
        error = "Missing SESSION.TIMEOUT"
        RETURN
    END
    
    agentID = SFF.MGram.GetCommon("AGENT.ID")
    IF NOT(agentID) THEN
        error = "Missing AGENT.ID"
        RETURN
    END
    
    agentSequence = SFF.MGram.GetCommon("AGENT.SEQUENCE")
    IF NOT(agentSequence) THEN
        error = "Missing AGENT.SEQUENCE"
        RETURN
    END
    
    token = SFF.MGram.GetCommon("TOKEN")
    IF NOT(token) THEN
        error = "Missing TOKEN"
        RETURN
    END
    
    apiVersion = SFF.MGram.GetCommon("API.VERSION")
    IF NOT(apiVersion) THEN
        error = "API.VERSION"
        RETURN
    END
    
    clientSoftwareVersion = SFF.MGram.GetCommon("CLIENT.SOFTWARE.VERSION")
    IF NOT(clientSoftwareVersion) THEN
        error = "CLIENT.SOFTWARE.VERSION"
        RETURN
    END
    
    channelType = SFF.MGram.GetCommon("CHANNEL.TYPE")
    IF NOT(channelType) THEN
        error = "CHANNEL.TYPE"
        RETURN
    END
    
    timeStamp = ""
    consumerId = "0"
    formFreeStaging = "false"
    
    GOSUB BuildHeaders
RETURN

BuildHeaders:
    headers = 'SOAPAction: urn:AgentConnect1512#sendValidation'
    headers := '^Content-Type: application/xml'
    
    GOSUB BuildPayload
RETURN

BuildPayload:
    request = 'requestType*getISODate'
    timeStamp = SFF.Util.InvokeEndPoint(request)
    IF NOT(timeStamp) THEN
        error = "Invalid Timestamp"
        RETURN
    END
    payload = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:agen="http://www.moneygram.com/AgentConnect1512">'
    payload := '<soapenv:Header/>'
    payload := '<soapenv:Body>'
    payload := '<agen:sendValidationRequest>'
    payload := '<agen:agentID>':agentID:'</agen:agentID>'
    payload := '<agen:agentSequence>':agentSequence:'</agen:agentSequence>'
    payload := '<agen:token>':token:'</agen:token>'
    payload := '<agen:timeStamp>':timeStamp:'</agen:timeStamp>'
    payload := '<agen:apiVersion>':apiVersion:'</agen:apiVersion>'
    payload := '<agen:clientSoftwareVersion>':clientSoftwareVersion:'</agen:clientSoftwareVersion>'
    payload := '<agen:channelType>':channelType:'</agen:channelType>'
    payload := '<agen:amount>':amount:'</agen:amount>'
    payload := '<agen:destinationCountry>':destinationCountry:'</agen:destinationCountry>'
    payload := '<agen:deliveryOption>':deliveryOption:'</agen:deliveryOption>'
    payload := '<agen:receiveCurrency>':receiveCurrency:'</agen:receiveCurrency>'
    payload := '<agen:senderFirstName>':senderFirstName:'</agen:senderFirstName>'
    payload := '<agen:senderLastName>':senderLastName:'</agen:senderLastName>'
    payload := '<agen:senderAddress>':senderAddress:'</agen:senderAddress>'
    payload := '<agen:senderCity>':senderCity:'</agen:senderCity>'
    payload := '<agen:senderCountry>':senderCountry:'</agen:senderCountry>'
    payload := '<agen:senderHomePhone>':phone:'</agen:senderHomePhone>'
    payload := '<agen:receiverFirstName>':receiverFirstName:'</agen:receiverFirstName>'
    payload := '<agen:receiverLastName>':receiverLastName:'</agen:receiverLastName>'
    payload := '<agen:senderPhotoIdType>':senderPhotoIdType:'</agen:senderPhotoIdType>'
    payload := '<agen:senderPhotoIdNumber>':senderPhotoIdNumber:'</agen:senderPhotoIdNumber>'
    payload := '<agen:senderPhotoIdCountry>':senderPhotoIdCountry:'</agen:senderPhotoIdCountry>'
    payload := '<agen:senderLegalIdType>':senderLegalIdType:'</agen:senderLegalIdType>'
    payload := '<agen:senderLegalIdNumber>':senderLegalIdNumber:'</agen:senderLegalIdNumber>'
    payload := '<agen:senderDOB>':senderDOB:'</agen:senderDOB>'
    payload := '<agen:senderOccupation>OTHER</agen:senderOccupation>'
    payload := '<agen:sendCurrency>':sendCurrency:'</agen:sendCurrency>'
    payload := '<agen:consumerId>':consumerId:'</agen:consumerId>'
    payload := '<agen:mgiTransactionSessionID>':sessionId:'</agen:mgiTransactionSessionID>'
    payload := '<agen:formFreeStaging>':formFreeStaging:'</agen:formFreeStaging>'
    payload := '<agen:sendPurposeOfTransaction>':txnPurpose:'</agen:sendPurposeOfTransaction>'
    payload := '<agen:sourceOfFunds>':fundSource:'</agen:sourceOfFunds>'
    payload := '<agen:relationshipToReceiver>':receiverRelation:'</agen:relationshipToReceiver>'
    payload := '<agen:senderOccupationOther>':senderOccupationOther:'</agen:senderOccupationOther>'
    payload := '<agen:senderCitizenshipCountry>':senderCitizenshipCountry:'</agen:senderCitizenshipCountry>'
    payload := '<agen:senderHomePhoneCountryCode>':countryCode:'</agen:senderHomePhoneCountryCode>'
    payload := '</agen:sendValidationRequest>'
    payload := '</soapenv:Body>'
    payload := '</soapenv:Envelope>'
    
    GOSUB SendRequest
RETURN

SendRequest:
    request = 'endPoint*':endPoint
    request := '|connectionTimeout*':connectionTimeout
    request := '|sessionTimeout*':sessionTimeout
    request := '|headers*':headers
    request := '|payload*':payload
    request := '|requestType*postRequest'
   
    response = SFF.Util.InvokeEndPoint(request)
    statusCode = LEFT(response,2)
    statusMessage = RIGHT(response, LEN(response)-3)
    IF statusCode NE "00" THEN
        error = statusMessage
        RETURN
    END
      
    statusCode = "05"
    IF INDEX(statusMessage,"<ac:errorString>",1) THEN
        str = "<ac:errorString>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</ac:errorString>",1)
        c = b - a
        error = statusMessage[a,c]
        RETURN
    END
    
    IF INDEX(statusMessage,"<ac:readyForCommit>",1) THEN
        str = "<ac:readyForCommit>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</ac:readyForCommit>",1)
        c = b - a
        
        IF TRIM(statusMessage[a,c]) NE "true" THEN
            error = "The transaction cannot be committed. Please try again later"
        END
    
        RETURN
    END
    
    error = "Invalid MoneyGram response. readyForCommit not found"
RETURN

END
