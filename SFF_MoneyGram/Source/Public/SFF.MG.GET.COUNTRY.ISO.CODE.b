* @ValidationCode : Mjo5NTI1ODQ3MjY6Q3AxMjUyOjE1OTYxNjAxMDIzMDM6TWFpbmE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMTlfU1AyNC4wOi0xOi0x
* @ValidationInfo : Timestamp         : 31 Jul 2020 04:48:22
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MGram
*
* Implementation of SFF.MGram.GetCountryIsoCode
*
* country(IN) :
*
FUNCTION SFF.MG.GET.COUNTRY.ISO.CODE(countryCode)
    $USING ST.Config
    $USING EB.LocalReferences

    GOSUB Init
RETURN(isoCode)

Init:
    isoCode = ""
    GOSUB Process
RETURN

Process:
    rec = ST.Config.Country.CacheRead(countryCode, err)
    IF err THEN
        isoCode = ""
    END

    appName = "COUNTRY"
    fieldName = "ISO.CODE"
    EB.LocalReferences.GetLocRef(appName, fieldName, pos)
    
    isoCode = rec<ST.Config.Country.EbCouLocalRef, pos>
RETURN

END