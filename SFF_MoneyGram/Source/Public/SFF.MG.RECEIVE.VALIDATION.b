* @ValidationCode : MjotMTgwNjE5NTA2MjpDcDEyNTI6MTU5Nzc5MjM3OTg4MzpNYWluYTotMTotMTowOjA6ZmFsc2U6Ti9BOlIxOV9TUDI0LjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 19 Aug 2020 02:12:59
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MGram
*
* Implementation of SFF.MGram.ReceiveValidation
*
* referenceNumber(IN) :
* receiveCurrency(IN) :
* agentCheckNumber(IN) :
* agentCheckAmount(IN) :
* receiverAddress(IN) :
* receiverCity(IN) :
* receiverCountry(IN) :
* receiverPhotoIdType(IN) :
* receiverPhotoIdNumber(IN) :
* receiverPhotoIdCountry(IN) :
* receiverLegalIdType(IN) :
* receiverLegalIdNumber(IN) :
* receiverPhoneCountryCode(IN) :
* receiverPhone(IN) :
* relationshipToSender(IN) :
* receiverCitizenshipCountry(IN) :
* sessionId(IN) :
* error(OUT) :
*
SUBROUTINE SFF.MG.RECEIVE.VALIDATION(referenceNumber, receiveCurrency, agentCheckNumber, agentCheckAmount, receiverAddress, receiverCity, receiverCountry, receiverPhotoIdType, receiverPhotoIdNumber, receiverPhotoIdCountry, receiverLegalIdType, receiverLegalIdNumber, receiverDoB, receiverPhoneCountryCode, receiverPhone, relationshipToSender, receiverCitizenshipCountry, sessionId, error)
    $USING SFF.Util
        
    GOSUB Init
RETURN
    
Init:
    IF NOT(referenceNumber) THEN
        error = "Missing Reference Number"
        RETURN
    END
     
    IF NOT(receiveCurrency ) THEN
        error = "Missing Receive Currency"
        RETURN
    END

    IF LEN(agentCheckNumber) EQ 0 THEN
        error = "Missing Agent Check Number"
        RETURN
    END
    
    IF NOT(agentCheckAmount) THEN
        error = "Missing Agent Check Amount"
        RETURN
    END

    IF NOT(receiverAddress) THEN
        error = "Missing Receiver Address"
        RETURN
    END
    
    IF NOT(receiverCity) THEN
        error = "Missing Receiver City"
        RETURN
    END

    IF NOT(receiverCountry) THEN
        error = "Missing Receiver Country"
        RETURN
    END

    IF NOT(receiverPhotoIdType) THEN
        error = "Missing Photo Id Type"
        RETURN
    END
    
    IF NOT(receiverPhotoIdNumber) THEN
        error = "Missing Photo Id Number"
        RETURN
    END
    
    IF NOT(receiverPhotoIdCountry) THEN
        error = "Missing Photo Id Country"
        RETURN
    END
    
    IF NOT(receiverLegalIdType) THEN
        error = "Missing Id Type"
        RETURN
    END
    
    IF NOT(receiverLegalIdNumber) THEN
        error = "Missing Id Number"
        RETURN
    END
    
    IF NOT(receiverDoB) THEN
        error = "Missing DOB"
        RETURN
    END
    
    IF NOT(receiverPhoneCountryCode) THEN
        error = "Missing Phone Country Code"
        RETURN
    END
    
    IF NOT(receiverPhone) THEN
        error = "Missing Phone"
        RETURN
    END
    
    IF NOT(relationshipToSender) THEN
        error = "Missing Relationship To Sender"
        RETURN
    END
    
    IF NOT(receiverCitizenshipCountry) THEN
        error = "Missing Nationality"
        RETURN
    END
    
    IF NOT(sessionId) THEN
        error = "Missing the Session Id"
        RETURN
    END
         
    endPoint = SFF.MGram.GetCommon("ENDPOINT")
    IF NOT(endPoint) THEN
        error = "Missing ENDPOINT"
        RETURN
    END
    
    connectionTimeout  = SFF.MGram.GetCommon("CONNECTION.TIMEOUT")
    IF NOT(connectionTimeout) THEN
        error = "Missing CONNECTION.TIMEOUT"
        RETURN
    END
    
    sessionTimeout  = SFF.MGram.GetCommon("SESSION.TIMEOUT")
    IF NOT(sessionTimeout) THEN
        error = "Missing SESSION.TIMEOUT"
        RETURN
    END
    
    agentID = SFF.MGram.GetCommon("AGENT.ID")
    IF NOT(agentID) THEN
        error = "Missing AGENT.ID"
        RETURN
    END
    
    agentSequence = SFF.MGram.GetCommon("AGENT.SEQUENCE")
    IF NOT(agentSequence) THEN
        error = "Missing AGENT.SEQUENCE"
        RETURN
    END
    
    token = SFF.MGram.GetCommon("TOKEN")
    IF NOT(token) THEN
        error = "Missing TOKEN"
        RETURN
    END
    
    apiVersion = SFF.MGram.GetCommon("API.VERSION")
    IF NOT(apiVersion) THEN
        error = "API.VERSION"
        RETURN
    END
    
    clientSoftwareVersion = SFF.MGram.GetCommon("CLIENT.SOFTWARE.VERSION")
    IF NOT(clientSoftwareVersion) THEN
        error = "CLIENT.SOFTWARE.VERSION"
        RETURN
    END
    
    channelType = SFF.MGram.GetCommon("CHANNEL.TYPE")
    IF NOT(channelType) THEN
        error = "CHANNEL.TYPE"
        RETURN
    END
    
    timeStamp = ""
    customerCheckNumber = "0000000000"
    customerCheckAmount = "0.000"
    consumerId = "0"
    formFreeStaging = "false"
    
    GOSUB BuildHeaders
RETURN

BuildHeaders:
    headers = 'SOAPAction: urn:AgentConnect1512#sendValidation'
    headers := '^Content-Type: application/xml'
    
    GOSUB BuildPayload
RETURN

BuildPayload:
    request = 'requestType*getISODate'
    timeStamp = SFF.Util.InvokeEndPoint(request)
    IF NOT(timeStamp) THEN
        error = "Invalid Timestamp"
        RETURN
    END
    
    payload := '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:agen="http://www.moneygram.com/AgentConnect1512">'
    payload := '<soapenv:Header/>'
    payload := '<soapenv:Body>'
    payload := '<agen:receiveValidationRequest>'
    payload := '<agen:agentID>':agentID:'</agen:agentID>'
    payload := '<agen:agentSequence>':agentSequence:'</agen:agentSequence>'
    payload := '<agen:token>':token:'</agen:token>'
    payload := '<agen:timeStamp>':timeStamp:'</agen:timeStamp>'
    payload := '<agen:apiVersion>':apiVersion:'</agen:apiVersion>'
    payload := '<agen:clientSoftwareVersion>':clientSoftwareVersion:'</agen:clientSoftwareVersion>'
    payload := '<agen:channelType>':channelType:'</agen:channelType>'
    payload := '<agen:referenceNumber>':referenceNumber:'</agen:referenceNumber>'
    payload := '<agen:receiveCurrency>':receiveCurrency:'</agen:receiveCurrency>'
    payload := '<agen:agentCheckNumber>':agentCheckNumber:'</agen:agentCheckNumber>'
    payload := '<agen:agentCheckAmount>':agentCheckAmount:'</agen:agentCheckAmount>'
    payload := '<agen:customerCheckNumber>':customerCheckNumber:'</agen:customerCheckNumber>'
    payload := '<agen:customerCheckAmount>':customerCheckAmount:'</agen:customerCheckAmount>'
    payload := '<agen:receiverAddress>':receiverAddress:'</agen:receiverAddress>'
    payload := '<agen:receiverCity>':receiverCity:'</agen:receiverCity>'
    payload := '<agen:receiverCountry>':receiverCountry:'</agen:receiverCountry>'
    payload := '<agen:receiverPhotoIdType>':receiverPhotoIdType:'</agen:receiverPhotoIdType>'
    payload := '<agen:receiverPhotoIdNumber>':receiverPhotoIdNumber:'</agen:receiverPhotoIdNumber>'
    payload := '<agen:receiverPhotoIdCountry>':receiverPhotoIdCountry:'</agen:receiverPhotoIdCountry>'
    payload := '<agen:receiverLegalIdType>':receiverLegalIdType:'</agen:receiverLegalIdType>'
    payload := '<agen:receiverLegalIdNumber>':receiverLegalIdNumber:'</agen:receiverLegalIdNumber>'
    payload := '<agen:receiverDOB>':receiverDoB:'</agen:receiverDOB>'
    payload := '<agen:consumerId>':consumerId:'</agen:consumerId>'
    payload := '<agen:receiverPhone>':receiverPhone:'</agen:receiverPhone>'
    payload := '<agen:mgiTransactionSessionID>':sessionId:'</agen:mgiTransactionSessionID>'
    payload := '<agen:formFreeStaging>':formFreeStaging:'</agen:formFreeStaging>'
    payload := '<agen:relationshipToSender>':relationshipToSender:'</agen:relationshipToSender>'
    payload := '<agen:receiverCitizenshipCountry>':receiverCitizenshipCountry:'</agen:receiverCitizenshipCountry>'
    payload := '<agen:receiverPhoneCountryCode>':receiverPhoneCountryCode:'</agen:receiverPhoneCountryCode>'
    payload := '</agen:receiveValidationRequest>'
    payload := '</soapenv:Body>'
    payload := '</soapenv:Envelope>'
    
    GOSUB SendRequest
RETURN

SendRequest:
    request = 'endPoint*':endPoint
    request := '|connectionTimeout*':connectionTimeout
    request := '|sessionTimeout*':sessionTimeout
    request := '|headers*':headers
    request := '|payload*':payload
    request := '|requestType*postRequest'
   
    response = SFF.Util.InvokeEndPoint(request)
    statusCode = LEFT(response,2)
    statusMessage = RIGHT(response, LEN(response)-3)
    IF statusCode NE "00" THEN
        error = statusMessage
        RETURN
    END
      
    statusCode = "05"
    IF INDEX(statusMessage,"<ac:errorString>",1) THEN
        str = "<ac:errorString>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</ac:errorString>",1)
        c = b - a
        error = statusMessage[a,c]
        RETURN
    END
    
    IF INDEX(statusMessage,"<ac:readyForCommit>",1) THEN
        str = "<ac:readyForCommit>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</ac:readyForCommit>",1)
        c = b - a
        
        IF TRIM(statusMessage[a,c]) NE "true" THEN
            error = "The transaction cannot be committed. Please try again later"
        END
    
        RETURN
    END
    
    error = "Invalid MoneyGram response. readyForCommit not found"
RETURN

END

