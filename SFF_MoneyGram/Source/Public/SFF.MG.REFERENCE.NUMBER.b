* @ValidationCode : MjoxMDY2MDU2ODY3OkNwMTI1MjoxNTk3NzkyNDAzMDc3Ok1haW5hOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjE5X1NQMjQuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Aug 2020 02:13:23
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MGram
*
* Implementation of SFF.MGram.ReferenceNumber
*
* referenceNumber(IN) :
*
SUBROUTINE SFF.MG.REFERENCE.NUMBER(referenceNumber, senderFirstName, senderLastName, senderHomePhone, receiverFirstName, receiverLastName, receiveCurrency, agentCheckNumber, agentCheckAmount, sessionId, error)
    $USING SFF.Util
    
    GOSUB Init
RETURN
    
Init:
    IF NOT(referenceNumber) THEN
        error = "Missing Reference Number"
        RETURN
    END
    
    endPoint = SFF.MGram.GetCommon("ENDPOINT")
    IF NOT(endPoint) THEN
        error = "Missing ENDPOINT"
        RETURN
    END
    
    connectionTimeout  = SFF.MGram.GetCommon("CONNECTION.TIMEOUT")
    IF NOT(connectionTimeout) THEN
        error = "Missing CONNECTION.TIMEOUT"
        RETURN
    END
    
    sessionTimeout  = SFF.MGram.GetCommon("SESSION.TIMEOUT")
    IF NOT(sessionTimeout) THEN
        error = "Missing SESSION.TIMEOUT"
        RETURN
    END
    
    agentID = SFF.MGram.GetCommon("AGENT.ID")
    IF NOT(agentID) THEN
        error = "Missing AGENT.ID"
        RETURN
    END
    
    agentSequence = SFF.MGram.GetCommon("AGENT.SEQUENCE")
    IF NOT(agentSequence) THEN
        error = "Missing AGENT.SEQUENCE"
        RETURN
    END
    
    token = SFF.MGram.GetCommon("TOKEN")
    IF NOT(token) THEN
        error = "Missing TOKEN"
        RETURN
    END
    
    apiVersion = SFF.MGram.GetCommon("API.VERSION")
    IF NOT(apiVersion) THEN
        error = "API.VERSION"
        RETURN
    END
    
    clientSoftwareVersion = SFF.MGram.GetCommon("CLIENT.SOFTWARE.VERSION")
    IF NOT(clientSoftwareVersion) THEN
        error = "CLIENT.SOFTWARE.VERSION"
        RETURN
    END
    
    channelType = SFF.MGram.GetCommon("CHANNEL.TYPE")
    IF NOT(channelType) THEN
        error = "CHANNEL.TYPE"
        RETURN
    END
        
    timeStamp = ""
    
    GOSUB BuildHeaders
RETURN

BuildHeaders:
    headers = 'SOAPAction: urn:AgentConnect1512#referenceNumber'
    headers := '^Content-Type: application/xml'
    GOSUB BuildPayload
RETURN

BuildPayload:
    request = 'requestType*getISODate'
    timeStamp = SFF.Util.InvokeEndPoint(request)
    IF NOT(timeStamp) THEN
        error = "Invalid Timestamp"
        RETURN
    END
               
    payload := '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:agen="http://www.moneygram.com/AgentConnect1512">'
    payload := '<soapenv:Body>'
    payload := '<agen:referenceNumberRequest>'
    payload := '<agen:agentID>':agentID:'</agen:agentID>'
    payload := '<agen:agentSequence>':agentSequence:'</agen:agentSequence>'
    payload := '<agen:token>':token:'</agen:token>'
    payload := '<agen:timeStamp>':timeStamp:'</agen:timeStamp>'
    payload := '<agen:apiVersion>':apiVersion:'</agen:apiVersion>'
    payload := '<agen:clientSoftwareVersion>':clientSoftwareVersion:'</agen:clientSoftwareVersion>'
    payload := '<agen:channelType>':channelType:'</agen:channelType>'
    payload := '<agen:referenceNumber>':referenceNumber:'</agen:referenceNumber>'
    payload := '</agen:referenceNumberRequest>'
    payload := '</soapenv:Body>'
    payload := '</soapenv:Envelope>'
    
    GOSUB SendRequest
RETURN

SendRequest:
    request = 'endPoint*':endPoint
    request := '|connectionTimeout*':connectionTimeout
    request := '|sessionTimeout*':sessionTimeout
    request := '|headers*':headers
    request := '|payload*':payload
    request := '|requestType*postRequest'
   
    response = SFF.Util.InvokeEndPoint(request)
    statusCode = LEFT(response,2)
    statusMessage = RIGHT(response, LEN(response)-3)
    IF statusCode NE "00" THEN
        error = statusMessage
        RETURN
    END
      
    statusCode = "05"
    IF INDEX(statusMessage,"<ac:errorString>",1) THEN
        str = "<ac:errorString>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</ac:errorString>",1)
        c = b - a
        error = statusMessage[a,c]
        RETURN
    END
    
    GOSUB GetSenderFirstName
RETURN

GetSenderFirstName:
    IF INDEX(statusMessage,"<ac:senderFirstName>",1) THEN
        str = "<ac:senderFirstName>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</ac:senderFirstName>",1)
        c = b - a
        senderFirstName = statusMessage[a,c]
    END
    
    GOSUB GetSenderLastName
RETURN

GetSenderLastName:
    IF INDEX(statusMessage,"<ac:senderLastName>",1) THEN
        str = "<ac:senderLastName>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</ac:senderLastName>",1)
        c = b - a
        senderLastName = statusMessage[a,c]
    END
    
    GOSUB GetSenderHomePhone
RETURN
        
GetSenderHomePhone:
    IF INDEX(statusMessage,"<ac:senderHomePhone>",1) THEN
        str = "<ac:senderHomePhone>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</ac:senderHomePhone>",1)
        c = b - a
        senderHomePhone = statusMessage[a,c]
    END
    
    GOSUB GetReceiverFirstName
RETURN
    
GetReceiverFirstName:
    IF INDEX(statusMessage,"<ac:receiverFirstName>",1) THEN
        str = "<ac:receiverFirstName>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</ac:receiverFirstName>",1)
        c = b - a
        receiverFirstName = statusMessage[a,c]
    END
    
    GOSUB GetReceiverLastName
RETURN
    
GetReceiverLastName:
    IF INDEX(statusMessage,"<ac:receiverLastName>",1) THEN
        str = "<ac:receiverLastName>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</ac:receiverLastName>",1)
        c = b - a
        receiverLastName = statusMessage[a,c]
    END
    
    GOSUB GetReceiveCurrency
RETURN

GetReceiveCurrency:
    IF INDEX(statusMessage,"<ac:receiveCurrency>",1) THEN
        str = "<ac:receiveCurrency>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</ac:receiveCurrency>",1)
        c = b - a
        receiveCurrency = statusMessage[a,c]
    END
    
    GOSUB GetAgentCheckNumber
RETURN
        
GetAgentCheckNumber:
    IF INDEX(statusMessage,"<ac:agentCheckNumber>",1) THEN
        str = "<ac:agentCheckNumber>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</ac:agentCheckNumber>",1)
        c = b - a
        agentCheckNumber = statusMessage[a,c]
    END
    
    GOSUB GetAgentCheckAmount
RETURN

GetAgentCheckAmount:
    IF INDEX(statusMessage,"<ac:agentCheckAmount>",1) THEN
        str = "<ac:agentCheckAmount>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</ac:agentCheckAmount>",1)
        c = b - a
        agentCheckAmount = statusMessage[a,c]
    END
    
    GOSUB GetSessionId
RETURN

GetSessionId:
    IF INDEX(statusMessage,"<ac:mgiTransactionSessionID>",1) THEN
        str = "<ac:mgiTransactionSessionID>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</ac:mgiTransactionSessionID>",1)
        c = b - a
        sessionId = statusMessage[a,c]
    END
    
RETURN
    
END
