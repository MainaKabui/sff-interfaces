* @ValidationCode : MjotMTUxODQ4MjgyMTpDcDEyNTI6MTU5Nzc5MjMyODE2NjpNYWluYTotMTotMTowOjA6ZmFsc2U6Ti9BOlIxOV9TUDI0LjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 19 Aug 2020 02:12:08
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MGram
*
* Implementation of SFF.MGram.CommitTransaction
*
* sessionId(IN) :
* error(OUT) :
*
SUBROUTINE SFF.MG.COMMIT.TRANSACTION(sessionId, productType, referenceNo, deliveryDate, error)
    $USING SFF.Util
    
    GOSUB Init
RETURN
    
Init:
    IF NOT(sessionId) THEN
        error = "Missing the Session Id"
        RETURN
    END
        
    endPoint = SFF.MGram.GetCommon("ENDPOINT")
    IF NOT(endPoint) THEN
        error = "Missing ENDPOINT"
        RETURN
    END
    
    connectionTimeout  = SFF.MGram.GetCommon("CONNECTION.TIMEOUT")
    IF NOT(connectionTimeout) THEN
        error = "Missing CONNECTION.TIMEOUT"
        RETURN
    END
    
    sessionTimeout  = SFF.MGram.GetCommon("SESSION.TIMEOUT")
    IF NOT(sessionTimeout) THEN
        error = "Missing SESSION.TIMEOUT"
        RETURN
    END
    
    agentID = SFF.MGram.GetCommon("AGENT.ID")
    IF NOT(agentID) THEN
        error = "Missing AGENT.ID"
        RETURN
    END
    
    agentSequence = SFF.MGram.GetCommon("AGENT.SEQUENCE")
    IF NOT(agentSequence) THEN
        error = "Missing AGENT.SEQUENCE"
        RETURN
    END
    
    token = SFF.MGram.GetCommon("TOKEN")
    IF NOT(token) THEN
        error = "Missing TOKEN"
        RETURN
    END
    
    apiVersion = SFF.MGram.GetCommon("API.VERSION")
    IF NOT(apiVersion) THEN
        error = "API.VERSION"
        RETURN
    END
    
    clientSoftwareVersion = SFF.MGram.GetCommon("CLIENT.SOFTWARE.VERSION")
    IF NOT(clientSoftwareVersion) THEN
        error = "CLIENT.SOFTWARE.VERSION"
        RETURN
    END
    
    channelType = SFF.MGram.GetCommon("CHANNEL.TYPE")
    IF NOT(channelType) THEN
        error = "CHANNEL.TYPE"
        RETURN
    END
    
    GOSUB BuildHeaders
RETURN

BuildHeaders:
    headers = 'SOAPAction: urn:AgentConnect1512#confirmToken'
    headers := '^Content-Type: application/xml'
    GOSUB BuildPayload
RETURN

BuildPayload:
    request = 'requestType*getISODate'
    timeStamp = SFF.Util.InvokeEndPoint(request)
    IF NOT(timeStamp) THEN
        error = "Invalid Timestamp"
        RETURN
    END
    
    payload = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:agen="http://www.moneygram.com/AgentConnect1512">'
    payload := '<soapenv:Header/>'
    payload := '<soapenv:Body>'
    payload := '<agen:commitTransactionRequest>'
    payload := '<agen:agentID>':agentID:'</agen:agentID>'
    payload := '<agen:agentSequence>':agentSequence:'</agen:agentSequence>'
    payload := '<agen:token>':token:'</agen:token>'
    payload := '<agen:timeStamp>':timeStamp:'</agen:timeStamp>'
    payload := '<agen:apiVersion>':apiVersion:'</agen:apiVersion>'
    payload := '<agen:clientSoftwareVersion>':clientSoftwareVersion:'</agen:clientSoftwareVersion>'
    payload := '<agen:channelType>':channelType:'</agen:channelType>'
    payload := '<agen:mgiTransactionSessionID>':sessionId:'</agen:mgiTransactionSessionID>'
    payload := '<agen:productType>':productType:'</agen:productType>'
    payload := '</agen:commitTransactionRequest>'
    payload := '</soapenv:Body>'
    payload := '</soapenv:Envelope>'

    GOSUB SendRequest
RETURN

SendRequest:
    request = 'endPoint*':endPoint
    request := '|connectionTimeout*':connectionTimeout
    request := '|sessionTimeout*':sessionTimeout
    request := '|headers*':headers
    request := '|payload*':payload
    request := '|requestType*postRequest'
   
    response = SFF.Util.InvokeEndPoint(request)
    statusCode = LEFT(response,2)
    statusMessage = RIGHT(response, LEN(response)-3)
    IF statusCode NE "00" THEN
        error = statusMessage
        RETURN
    END
      
    statusCode = "05"
    IF INDEX(statusMessage,"<ac:errorString>",1) THEN
        str = "<ac:errorString>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</ac:errorString>",1)
        c = b - a
        error = statusMessage[a,c]
        RETURN
    END
      
    GOSUB GetReferenceNo
RETURN

GetReferenceNo:
    IF INDEX(statusMessage,"<ac:referenceNumber>",1) THEN
        str = "<ac:referenceNumber>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</ac:referenceNumber>",1)
        c = b - a
        referenceNo = statusMessage[a,c]
        GOSUB GetDeliveryDate
    END ELSE
        error = "Reference Number not found"
        RETURN
    END
RETURN

GetDeliveryDate:
    IF INDEX(statusMessage,"<ac:expectedDateOfDelivery>",1) THEN
        str = "<ac:expectedDateOfDelivery>"
        a = INDEX(statusMessage,str,1) + LEN(str)
        b = INDEX(statusMessage,"</ac:expectedDateOfDelivery>",1)
        c = b - a
        deliveryDate = statusMessage[a,c]
        CHANGE '-' TO '' IN deliveryDate
    END
RETURN

END