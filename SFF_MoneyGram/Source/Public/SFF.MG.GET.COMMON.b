* @ValidationCode : MjoyMDI5MDMzOTk4OkNwMTI1MjoxNTk3NzkwMTg0NDk0Ok1haW5hOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjE5X1NQMjQuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Aug 2020 01:36:24
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SFF.MGram
*
* Implementation of SFF.MGram.GetCommon
*
*
FUNCTION SFF.MG.GET.COMMON(key)
    $USING SFF.Util
    
    GOSUB Init
RETURN (val)
    
Init:
    rec = SFF.Util.SFFCommon.CacheRead("MONEYGRAM", err)
    keys = rec<SFF.Util.SFFCommon.ParameterName>
    val = ""

    GOSUB GetValue
RETURN

GetValue:
    CHANGE @VM TO @FM IN keys
    LOCATE key IN keys SETTING pos THEN
        val = rec<SFF.Util.SFFCommon.ParameterValue, pos>
    END
RETURN
    
END